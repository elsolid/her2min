import React from 'react'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import MainStackNavigator from './MainStackNavigator'
import LoginScreen from '../screens/Login'
import Disclaimer from '../screens/Disclaimer'
import AuthLoadingScreen from '../screens/AuthLoadingScreen'

export default createAppContainer(
  createSwitchNavigator(
    {
      Main: MainStackNavigator,
      Login: LoginScreen,
      Disclaimer: Disclaimer,
      AuthLoading: AuthLoadingScreen
    },
    {
      initialRouteName: 'AuthLoading'
    }
  )
)
