import React from 'react'
import { View, Text } from 'react-native'
import {
  createStackNavigator,
  createDrawerNavigator,
  DrawerItems,
  SafeAreaView
} from 'react-navigation'

import HomeScreen from '../screens/HomeScreen'
import IconF from 'react-native-vector-icons/FontAwesome'
import color from '../constants/Colors'

import Hercepin from './HercepinStack'
import Kadcyla from './KadcylaStack'
import Perjeta from './PerjetaStack'

const StackNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    hercepin: Hercepin,
    kadcyla: Kadcyla,
    perjeta: Perjeta
  },
  {
    headerLayoutPreset: 'center',
    headerMode: 'none'
  }
)

export default StackNavigator
