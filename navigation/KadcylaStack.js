import React from 'react'
import {
  createStackNavigator,
  createDrawerNavigator,
  NavigationActions
} from 'react-navigation'
// kadcyla
import MainKadcyla from '../screens/kadcyla/view.1'
import KadcylaView2 from '../screens/kadcyla/view.2'
import KadcylaView3 from '../screens/kadcyla/view.3'
import KadcylaView4 from '../screens/kadcyla/view.4'
import KadcylaView5 from '../screens/kadcyla/view.5'
import KadcylaView6 from '../screens/kadcyla/view.6'

import SideIndicaciones from '../screens/sidemenu/kadcyla/SideIndicaciones'
import SideMateriales from '../screens/sidemenu/kadcyla/SideMateriales'
import SideReferencias from '../screens/sidemenu/kadcyla/SideReferencias'
import SideFarmacoVigilancia from '../screens/sidemenu/kadcyla/SideFarmacoVigilancia'

import IconF from 'react-native-vector-icons/FontAwesome'
import color from '../constants/Colors'
import CustomDrawerComponent from '../components/CustomDrawerComponent'

// const indicaciones = () => (<SideIndicaciones title='probandndnd' />)

const Kadcyla = createStackNavigator(
  {
    // view1: { screen: MainKadcyla },
    view2: { screen: KadcylaView2 },
    view3: { screen: KadcylaView3 },
    view4: { screen: KadcylaView4 },
    view5: { screen: KadcylaView5 },
    view6: { screen: KadcylaView6 },
    ksideindicaciones: {screen: SideIndicaciones},
    ksidemateriales: {screen: SideMateriales },
    ksidereferencias: {screen: SideReferencias},
    ksideFarmacoVigilancia: {screen: SideFarmacoVigilancia}
  },
  {
    headerLayoutPreset: 'center'
  }
)

export default createDrawerNavigator(
  {
    Inicio: {
      screen: Kadcyla,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='home'
            size={24}
            style={{ color: color.kadcylaPrimary }}
          />
        )
      }
    },
    ksideindicaciones: {
      screen: Kadcyla,
      navigationOptions: {
        title: 'Indicaciones',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='medkit'
            size={24}
            style={{ color: color.kadcylaPrimary }}
          />
        )
      }
    },
    ksidemateriales: {
      screen: Kadcyla,
      navigationOptions: {
        title: 'Materiales',
        // onItemPress: route => {
        //   alert(JSON.stringify(route))
        // }, // NavigationActions.navigate('Home'),
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='plus-square'
            size={24}
            style={{ color: color.kadcylaPrimary }}
          />
        )
      }
    },
    ksideFarmacoVigilancia: {
      screen: Kadcyla,
      navigationOptions: {
        title: 'Farmacovigilancia',
        drawerIcon: ({ tintColor }) => (
          <IconF name='eye' size={24} style={{ color: color.kadcylaPrimary }} />
        )
      }
    },
    Reporte: {
      screen: Kadcyla,
      navigationOptions: {
        title: 'Reporte de eventos adversos',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='envelope'
            size={24}
            style={{ color: color.kadcylaPrimary }}
          />
        )
      }
    },
    ksidereferencias: {
      screen: Kadcyla,
      navigationOptions: {
        title: 'Referencias',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='file'
            size={24}
            style={{ color: color.kadcylaPrimary }}
          />
        )
      }
    },
    Cerrar: {
      screen: Kadcyla,
      navigationOptions: {
        title: 'Cerrar sesion',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='lock'
            size={24}
            style={{ color: color.kadcylaPrimary }}
          />
        )
      }
    }
  },
  {
    contentComponent: CustomDrawerComponent
  }
)
