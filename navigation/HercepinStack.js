import React from 'react'
import { createStackNavigator, createDrawerNavigator } from 'react-navigation'
import { View, StyleSheet, TouchableOpacity } from 'react-native'

// hercepin
import HercepinView1 from '../screens/hercepin/view.1'
import HercepinView2 from '../screens/hercepin/view.2'
import HercepinView3 from '../screens/hercepin/view.3'
import HercepinView4 from '../screens/hercepin/view.4'
import HercepinView5 from '../screens/hercepin/view.5'
import HercepinView6 from '../screens/hercepin/view.6'
import HercepinView7 from '../screens/hercepin/view.7'
import HercepinView8 from '../screens/hercepin/view.8'

import SideIndicaciones from '../screens/sidemenu/hercepin/SideIndicaciones'
import SideMateriales from '../screens/sidemenu/hercepin/SideMateriales'
import SideReferencias from '../screens/sidemenu/hercepin/SideReferencias'
import SideFarmacoVigilancia from '../screens/sidemenu/hercepin/SideFarmacoVigilancia'

import IconF from 'react-native-vector-icons/FontAwesome'
import color from '../constants/Colors'
import CustomDrawerComponent from '../components/CustomDrawerComponent'

const Hercepin = createStackNavigator(
  {
    hercepin1: { screen: HercepinView1 },
    hercepin2: { screen: HercepinView2 },
    hercepin3: { screen: HercepinView3 },
    hercepin4: { screen: HercepinView4 },
    hercepin5: { screen: HercepinView5 },
    hercepin6: { screen: HercepinView6 },
    hercepin7: { screen: HercepinView7 },
    hercepin8: { screen: HercepinView8 },
    hsideindicaciones: {screen: SideIndicaciones},
    hsidemateriales: {screen: SideMateriales },
    hsidereferencias: {screen: SideReferencias},
    hsideFarmacoVigilancia: {screen: SideFarmacoVigilancia}
  },
  {
    headerLayoutPreset: 'center'
  }
)

export default createDrawerNavigator(
  {
    Inicio: {
      screen: Hercepin,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='home'
            size={24}
            style={{ color: color.hercepinPrimary }}
          />
        )
      }
    },
    hsideindicaciones: {
      screen: Hercepin,
      navigationOptions: {
        title: 'Indicaciones',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='medkit'
            size={24}
            style={{ color: color.hercepinPrimary }}
          />
        )
      }
    },
    hsidemateriales: {
      screen: Hercepin,
      navigationOptions: {
        title: 'Materiales',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='plus-square'
            size={24}
            style={{ color: color.hercepinPrimary }}
          />
        )
      }
    },
    hsideFarmacoVigilancia: {
      screen: Hercepin,
      navigationOptions: {
        title: 'Farmacovigilancia',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='eye'
            size={24}
            style={{ color: color.hercepinPrimary }}
          />
        )
      }
    },
    Reporte: {
      screen: Hercepin,
      navigationOptions: {
        title: 'Reporte de eventos adversos',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='envelope'
            size={24}
            style={{ color: color.hercepinPrimary }}
          />
        )
      }
    },
    hsidereferencias: {
      screen: Hercepin,
      navigationOptions: {
        title: 'Referencias',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='file'
            size={24}
            style={{ color: color.hercepinPrimary }}
          />
        )
      }
    },
    Cerrar: {
      screen: HercepinView1,
      navigationOptions: {
        title: 'Cerrar sesion',
        drawerIcon: ({ tintColor }) => (
          <IconF
            name='lock'
            size={24}
            style={{ color: color.hercepinPrimary }}
          />
        )
      }
    }
  },
  {
    contentComponent: CustomDrawerComponent
  }
)
