import React from 'react'
import { createStackNavigator, createDrawerNavigator } from 'react-navigation'
// perjeta
import PerjetaView1 from '../screens/perjeta/view.1'
import PerjetaView2 from '../screens/perjeta/view.2'
import PerjetaView3 from '../screens/perjeta/view.3'

import SideIndicaciones from '../screens/sidemenu/perjeta/SideIndicaciones'
import SideMateriales from '../screens/sidemenu/perjeta/SideMateriales'
import SideReferencias from '../screens/sidemenu/perjeta/SideReferencias'
import SideFarmacoVigilancia from '../screens/sidemenu/perjeta/SideFarmacoVigilancia'

import IconF from 'react-native-vector-icons/FontAwesome'
import color from '../constants/Colors'
import CustomDrawerComponent from '../components/CustomDrawerComponent'

const Perjeta = createStackNavigator(
  {
    perjeta1: { screen: PerjetaView1 },
    perjeta2: { screen: PerjetaView2 },
    perjeta3: { screen: PerjetaView3 },
    psideindicaciones: {screen: SideIndicaciones},
    psidemateriales: {screen: SideMateriales },
    psidereferencias: {screen: SideReferencias},
    psideFarmacoVigilancia: {screen: SideFarmacoVigilancia}
  },
  {
    headerLayoutPreset: 'center'
  }
)

export default createDrawerNavigator(
  {
    Inicio: {
      screen: Perjeta,
      navigationOptions: {
        drawerIcon: () => (
          <IconF
            name='home'
            size={24}
            style={{ color: color.perjetaPrimary }}
          />
        )
      }
    },
    psideindicaciones: {
      screen: PerjetaView2,
      navigationOptions: {
        title: 'Indicaciones',
        drawerIcon: () => (
          <IconF
            name='medkit'
            size={24}
            style={{ color: color.perjetaPrimary }}
          />
        )
      }
    },
    psidemateriales: {
      screen: PerjetaView3,
      navigationOptions: {
        title: 'Materiales',
        drawerIcon: () => (
          <IconF
            name='plus-square'
            size={24}
            style={{ color: color.perjetaPrimary }}
          />
        )
      }
    },
    psideFarmacoVigilancia: {
      screen: PerjetaView2,
      navigationOptions: {
        title: 'Farmacovigilancia',
        drawerIcon: () => (
          <IconF name='eye' size={24} style={{ color: color.perjetaPrimary }} />
        )
      }
    },
    Reporte: {
      screen: PerjetaView2,
      navigationOptions: {
        title: 'Reporte de eventos adversos',
        drawerIcon: () => (
          <IconF
            name='envelope'
            size={24}
            style={{ color: color.perjetaPrimary }}
          />
        )
      }
    },
    psidereferencias: {
      screen: Perjeta,
      navigationOptions: {
        title: 'Referencias',
        drawerIcon: () => (
          <IconF
            name='file'
            size={24}
            style={{ color: color.perjetaPrimary }}
          />
        )
      }
    },
    Cerrar: {
      screen: PerjetaView2,
      navigationOptions: {
        title: 'Cerrar sesion',
        drawerIcon: () => (
          <IconF
            name='lock'
            size={24}
            style={{ color: color.perjetaPrimary }}
          />
        )
      }
    }
  },
  {
    gesturesEnabled: true,
    contentComponent: CustomDrawerComponent
  }
)
