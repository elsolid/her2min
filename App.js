import React from 'react'
import { createAppContainer } from 'react-navigation'
import { Provider } from 'react-redux'
import createStore from './redux'

// We're going to use navigation with redux
import ReduxNavigation from './navigation/ReduxNavigation'

// create our store
const store = createStore()

import AppNavigator from './navigation/AppNavigator'

const AppContainer = createAppContainer(AppNavigator)

export default class App extends React.Component {
  render () {
    return <AppContainer />
    // return (
    //   <Provider store={store}>
    //     <ReduxNavigation />
    //   </Provider>
    // )
  }
}
