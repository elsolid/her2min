import React, { Component } from 'react'
import { View, Text } from 'react-native'

import { Button } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/EvilIcons'
import IconF from 'react-native-vector-icons/FontAwesome'
import { DrawerActions } from 'react-navigation'

import color from '../constants/Colors'
import layout from '../constants/Layout'

export default (HeaderNavigator = props => ({
  // title: props.title,
  headerBackground: (
    <LinearGradient
      colors={props.colors}
      style={{ flex: 1 }}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
    />
  ),
  headerStyle:{
    height: 75
  },
  headerRight: (
    <Button
      // onPress={() => props.navigation.toggleDrawer()}
      // onPress={() => props.navigation.navigate('Drawer')}
      onPress={() => {
        props.navigation.dispatch(DrawerActions.toggleDrawer())
      }}
      icon={<IconF name='bars' size={30} color='white' />}
      type='clear'
      color='#fff'
    />
  ),
  headerLeft: (
    <Button
      onPress={() => props.navigation.navigate('Home')}
      icon={<IconF name='home' size={30} color='white' />}
      type='clear'
      color='#fff'
    />
  ),
  headerTitle: (
    <Text
      style={{
        textAlign: 'center',
        flex: 1,
        fontSize: 26,
        color: 'white',
        fontWeight: 'bold'
      }}
    >
      {props.title}

      {props.subtitle && (
        <Text style={{ fontSize: 18, marginBottom: 10, fontWeight: "normal" }}>{`\n`}{props.subtitle}</Text>
      )}
    </Text>
  ),
  headerTintColor: '#fff'
}))
