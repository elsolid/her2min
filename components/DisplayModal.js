import React from 'react'
import {
  Modal,
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import layout from '../constants/Layout'

const DisplayModal = props => (
  <Modal
    visible={props.display}
    animationType='slide'
    onRequestClose={props.toggleModal}
    transparent
  >
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)'
      }}
    >
      <View
        style={{
          width: layout.window.width * 0.8,
          height: layout.window.width * 0.6,
          backgroundColor: '#fff',
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 10,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84
        }}
      >
        <Text
          style={{
            fontSize: 28,
            color: props.colors[0],
            alignSelf: 'center',
            marginTop: 12
          }}
        >
          ¡Cuidado!
        </Text>
        <Text style={styles.text}>
          {props.data
            ? props.data
            : 'La infusión se debe de administrar de inmediato'}
        </Text>
        <TouchableOpacity
          style={{
            marginTop: 16,
            marginRight: 10,
            width: '80%',
            alignSelf: 'center'
          }}
          onPress={() => props.toggleModal()}
        >
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={props.colors}
            style={{ borderRadius: 20 }}
          >
            <Text style={styles.buttonText}>ENTENDIDO</Text>
          </LinearGradient>
        </TouchableOpacity>
        {/* <TouchableHighlight
          onPress={() => {
            props.toggleModal()
            // alert('hola')
            // props.navigation.navigate('Home')
          }}
        >
          <Text
            style={{
              fontSize: 22,
              color: props.color,
              alignSelf: 'center',
              marginTop: 30
            }}
          >
            ENTENDIDO
          </Text>
        </TouchableHighlight> */}
      </View>
    </View>
  </Modal>
)

const styles = StyleSheet.create({
  image: {
    marginTop: 20,
    marginLeft: 90,
    height: 200,
    width: 200
  },
  text: {
    fontSize: 18,
    marginTop: 25,
    alignSelf: 'center',
    width: '80%',
    textAlign: 'center'
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})

export default DisplayModal
