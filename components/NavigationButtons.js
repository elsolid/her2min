import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Input, ListItem } from 'react-native-elements'
import color from '../constants/Colors.js'
import Icon from 'react-native-vector-icons/EvilIcons'

export default class NavigationButtons extends Component {
  render () {
    let { anterior, siguiente, navigation, colors, params } = this.props

    return (
      <View
        style={{
          flexGrow: 1,
          alignItems: 'center',
          justifyContent: 'space-between',
          flexDirection: 'row'
        }}
      >
        {anterior.visible ? (
          <TouchableOpacity
            style={{
              // marginTop: 16,
              marginLeft: 10,
              width: '40%'
            }}
            onPress={() => navigation.navigate(anterior.view)}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={colors}
              style={{ borderRadius: 20 }}
            >
              <Text style={styles.buttonText}>Anterior</Text>
            </LinearGradient>
          </TouchableOpacity>
        ) : null}
        {siguiente.visible ? (
          <TouchableOpacity
            style={{
              // marginTop: 16,
              marginRight: 10,
              width: '40%'
            }}
            onPress={() => navigation.navigate(siguiente.view, {...params})}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={colors}
              style={{ borderRadius: 20 }}
            >
              <Text style={styles.buttonText}>Siguiente</Text>
            </LinearGradient>
          </TouchableOpacity>
        ) : null}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  linearGradientAnterior: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 20,
    marginTop: 16,
    width: '40%',
    elevation: 10,
    marginLeft: 10,
    alignSelf: 'flex-start'
  },
  linearGradientSiguiente: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 20,
    marginTop: 16,
    width: '40%',
    elevation: 10,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
