import React from 'react'
import { Linking, AsyncStorage, Platform } from 'react-native'
import { DrawerItems, SafeAreaView } from 'react-navigation'
import Correo from '../constants/Correo'

_signOutAsync = async props => {
  await AsyncStorage.clear()
  props.navigation.navigate('AuthLoading')
}

const CustomDrawerContentComponent = props => (
  <SafeAreaView style={{ marginTop: 45 }}>
    <DrawerItems
      {...props}
      onItemPress={route => {
        // alert(JSON.stringify(route))
        if (route.route.routeName == 'Reporte') {
          let url = (Platform.OS === 'android')
            ? `${Correo.address}?subject=${Correo.subject}&body=${Correo.message}`
            : `${Correo.address}&subject=${Correo.subject}&body=${Correo.message}`;


          return Linking.canOpenURL(url)
            .then(supported => {
              if (!supported) {
                console.log('Unsupported URL: ' + url)
              } else {
                return Linking.openURL(url)
              }
            })
            .catch(err => console.error('An error occurred ', err))
        } else if (route.route.routeName == 'Cerrar') {
          this._signOutAsync(props)
        } else if (route.route.routeName == 'Inicio') {
          props.navigation.navigate('Home')
        } else {
          // props.navigation.navigate()
          props.onItemPress(route)
        }
      }}
    />
  </SafeAreaView>
)

export default CustomDrawerContentComponent
