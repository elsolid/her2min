const indicaciones =
  'Cáncer de mama metastásico (CMm). Kadcyla, como agente idividual, está indicado para el tratamiento de pacientes con cáncer de mama HER2 positivo, no resecable, localmente avanzado o cáncer de mama metastásico, que han recibido tratamiento previo con trastuzumab y un taxano.'
const materiales = [
  'Campana de flujo laminar',
  'Guantes de nitrilo',
  'Gorro desechable',
  'Mascarilla',
  'Googles',
  'Bata desechable',
  'Jeringas',
  'Agujas',
  'Alcohol al 70%',
  'Gasas',
  'Venopak',
  'Bombas de infusión',
  'Solución salina al 0.9% de 250 ml',
  'Filtro para Trastuzumab emtansina'
]

const farmacovigilancia = [
  'Son las actividades realcionadas con la detección, evaluación, comprensión y prevención de los eventos adversos, las sospechas de reacciones adversas, las reacciones adversas, los eventos supuestamente atribuibles a la vacunación o inmunización, o cualquier otro problema de seguridad realcionado con el uso de los medicamentos y vacunas.',
  'A través de la información que se genera de estos reportes de eventos adversos se se obtiene información muy valiosa con respecto a la seguridad de los medicamentos en la práctica clínica habitual, lo que permite seguir alimentando el perfil de seguridad de los medicamentos, y así identificar riesgos de manera oportuna para prevenir daños en los pacientes, por lo tanto, garantizar que los perfiles de seguridad de los medicamentos sigan siendo favorable.',
  'Recuerde que también es importante reportar cualquier situación especial con el uso de medicamentos, por ejemplo\n- Embarazo y lactancia durante la exposición a medicamentos\n- Falta de eficacia\n-Progresión de la enfermedad de manera atípica o acelerada\n- Errores de medicación\n- Uso fuera de indicación\n- Interacciones medicamentosas ',
  'Y todas aquellas situaciones especiales\nque se llegen a presentar durante el tratamiento.'
]
const referencias =
  'Norma Oficial Mexicana NOM-220-SSA1-2016, Instalación y Operación de la Farmacovigilancia. Publicada en el DOF el 19 de Julio de 2017.'

const disclaimer = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'

export default {
  indicaciones,
  materiales,
  farmacovigilancia,
  referencias,
  disclaimer
}
