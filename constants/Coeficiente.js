export default {
  k_dosis: 3.6,
  h_tri_dosis: 6,
  h_sem_dosis: 2,
  k_60_ml: 8,
  k_00_ml: 5,
  h_dilucion: 21,
  k_dilucion: 20
}
