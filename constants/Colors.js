const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: '#fceaf6',
  noticeText: '#f6eff4',
  kadcylaPrimary: '#802868',
  kadcylaSecondary: '#bc2893',
  kadcylaTitle: '#a13388',
  hercepinPrimary: '#f39a0b',
  hercepinSecondary: '#fab341',
  perjetaPrimary: '#9ac600',
  perjetaSecondary: '#b4e509',
  generalBlue: '#4690d9'
};
