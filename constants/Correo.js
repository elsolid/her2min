export default {
    address: 'mailto:mexico.info@roche.com',
    subject: 'REPORTE DE EVENTO ADVERSO_PMR',
    message: 'Por favor, en el mail escriba los siguientes datos:\n\n1. Género, edad y tipo de cáncer del paciente.\n2. Nombre, lote y dosis del medicamento\n3. Otros medicamentos que consume\n4. Evento adverso presentado; signos y síntomas que presentó, si se dio algún tratamiento para ello y cuál fue el desenlace del evento.\n\nO comuníquese a los siguientes teléfonos, para ser atendido por nuestra Unidad de Farmacovigilancia:\n\nCDMX: (55) 5258 5225\nLADA sin costo: 01 800 821 8887\n¡MUCHAS GRACIAS!\nNota: Todos sus datos en el reporte son confidenciales.'
}