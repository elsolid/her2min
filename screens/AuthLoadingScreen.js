import React, { Component } from 'react'
import {
  AsyncStorage,
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  View
} from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import color from '../constants/Colors'

export default class AuthLoadingScreen extends Component {
  constructor (props) {
    super(props)
    this._bootstrapAsync()
  }

  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken')
    this.props.navigation.navigate(userToken ? 'Home' : 'Login')
  }

  componentDidMount = () => {
    setTimeout(() => {
      SplashScreen.hide()
    }, 2500)
  }
  // Render any loading content that you like here
  render () {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator size={'large'} color={color.generalBlue} />
      </View>
    )
  }
}
