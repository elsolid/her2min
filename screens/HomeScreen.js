import React, { Component } from 'react'
import Svg, { Ellipse } from 'react-native-svg'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView
} from 'react-native'
import { Button } from 'react-native-elements'
import { createStackNavigator, createAppContainer } from 'react-navigation'
import color from '../constants/Colors'
import SplashScreen from 'react-native-splash-screen'

export default class Home extends Component {
  static navigationOptions = {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
      SplashScreen.hide()
    }, 3000)
  }

  render () {
    return (
      <ScrollView>
        <View
          style={{
            flexGrow: 1,
            alignItems: 'center',
            flexDirection: 'column'
            // justifyContent:'flex-end'
          }}
        >
          <Image
            source={require('../assets/images/home.banner.png')}
            style={{
              alignSelf: 'center',
              marginTop: 0,
              marginBottom: 10
            }}
          />

          <TouchableOpacity
            style={{
              width: '70%',
              height: 100,
              borderRadius: 12,
              // elevation: 4,
              alignItems: 'center',
              justifyContent: 'center',
              margin: 15,
              marginTop: 15,
              borderRightWidth: 1,
              borderBottomWidth: 1,
              borderRightColor: color.tabIconDefault,
              borderBottomColor: color.tabIconDefault,
              borderLeftWidth: 6,
              borderLeftColor: color.kadcylaPrimary
            }}
            onPress={() => this.props.navigation.navigate('kadcyla')}
          >
            <Image
              source={require('../assets/images/home.kadcyla.png')}
              style={{ flex: 1, width: '70%' }}
              resizeMode='contain'
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '70%',
              height: 100,
              borderRadius: 12,
              alignItems: 'center',
              justifyContent: 'center',
              margin: 15,
              borderRightWidth: 1,
              borderBottomWidth: 1,
              borderRightColor: color.tabIconDefault,
              borderBottomColor: color.tabIconDefault,
              borderLeftWidth: 6,
              borderLeftColor: color.hercepinPrimary
            }}
            onPress={() => this.props.navigation.navigate('hercepin')}
          >
            <Image
              source={require('../assets/images/home.hercepin.png')}
              style={{ flex: 1, width: '70%' }}
              resizeMode='contain'
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '70%',
              height: 100,
              borderRadius: 12,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 15,
              marginBottom: 50,
              borderRightWidth: 1,
              borderBottomWidth: 1,
              borderRightColor: color.tabIconDefault,
              borderBottomColor: color.tabIconDefault,
              borderLeftWidth: 6,
              borderLeftColor: color.perjetaPrimary
            }}
            onPress={() => this.props.navigation.navigate('perjeta')}
          >
            <Image
              source={require('../assets/images/home.perjeta.png')}
              style={{ flex: 1, width: '45%' }}
              resizeMode='contain'
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: 'white',
    flex: 1
  },
  button: {
    top: '28%',
    height: 116.43,
    borderRadius: 25
  },
  button2: {
    top: '28%',
    height: 116.43,
    borderRadius: 25
  },
  button3: {
    top: '28%',
    height: 116.43,
    borderRadius: 25
  }
})
