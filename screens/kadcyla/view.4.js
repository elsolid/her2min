import React, { Component } from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import {
  Button,
  Text,
  Input,
  ListItem,
  Image,
  Divider
} from 'react-native-elements'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import color from '../../constants/Colors'
import Coeficiente from '../../constants/Coeficiente'
import layout from '../../constants/Layout'
import HeaderNavigator from '../../components/HeaderNavigator'
import Toast, { DURATION } from 'react-native-easy-toast'

// const image = require('../assets/images/kadcyla3.png')

export default class MainKadcyla extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Kadcyla',
      colors: [color.kadcylaPrimary, color.kadcylaSecondary],
      navigation: navigation
    })

  constructor (props) {
    super(props)
    this.state = {
      dilucion: this.props.navigation.getParam('dilucion', 0),
      frascos: this.props.navigation.getParam('frascos', 0)
    }
  }

  componentDidMount = () => {
    let { frascos, dilucion } = this.state

    let d60 = dilucion - frascos.c60 * Coeficiente.k_60_ml
    let d00 = dilucion - d60

    this.setState({ d00: d00, d60: d60 })
    // this.refs.toast.show(
    //   <View
    //     style={{
    //       flexGrow: 1,
    //       height: layout.window.height * 0.12,
    //       backgroundColor: color.noticeText
    //     }}
    //   >
    //     {/* <Text
    //       h5
    //       style={{
    //         textAlign: 'center',
    //         marginTop: 5,
    //         marginLeft: 15,
    //         marginRight: 15
    //       }}
    //     >
    //       Recuerde, si usa solución NaCl al 0.9% deberá utilizar un fitro en
    //       linea
    //     </Text> */}
    //     <Text
    //       h3
    //       style={{
    //         textAlign: 'center',
    //         color: color.kadcylaPrimary,
    //         marginLeft: 15,
    //         marginRight: 15,
    //         textTransform: 'uppercase'
    //       }}
    //     >
    //       No utilice solución dextrosa 5%
    //     </Text>
    //   </View>
    // )
  }

  render () {
    return (
      // <View>
      <ScrollView
        onScroll={() => {
          this.refs.toast.close()
        }}
        // style={{ marginTop: 15 }}
      >
        <Text style={{ marginTop: 15, ...styles.title }}>Dilución </Text>
        <Text style={styles.text2}>
          Tome los mililitros señalados de cada frasco para la solución.
        </Text>
        <Image
          source={require('../../assets/images/kadcyla4.png')}
          style={{ alignSelf: 'center', marginTop: 10 }}
        />
        <View
          style={{
            flexGrow: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            marginBottom: 15
          }}
        >
          <View
            style={{
              width: '40%',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              // backgroundColor: 'blue'
            }}
          >
            <Image source={require('../../assets/images/kadcyla4.4.png')} />
            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 3 }}>
              {this.state.d60 < 0
                ? 0
                : Number.parseFloat(this.state.d60).toFixed(2)}{' '}
              ml
            </Text>
          </View>

          <View
            style={{
              width: '50%',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              // backgroundColor: 'red',
            }}
          >
            <Image source={require('../../assets/images/kadcyla4.4.png')} />
            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 3 }}>
              {this.state.d00} ml
            </Text>
          </View>
        </View>
        <Text
          style={{
            alignSelf: 'center',
            textAlign: 'center',
            fontSize: 18,
            width: '65%'
          }}
        >
          Volumen total de la solución reconstituida a diluir
        </Text>
        <View
          style={{
            flexGrow: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            margin: 15
          }}
        >
          <Image source={require('../../assets/images/kadcyla4.4.png')} />
          <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 3 }}>
            {Number.parseFloat(this.state.dilucion).toFixed(2)} ml
          </Text>
        </View>
        <Text
          style={{
            alignSelf: 'center',
            textAlign: 'center',
            fontSize: 18,
            width: '65%',
            color: color.kadcylaPrimary
          }}
        >
          ¡No utilice solución dextrosa 5%!
        </Text>

        <Text
          style={{
            alignSelf: 'center',
            textAlign: 'center',
            fontSize: 18,
            width: '65%'
          }}
        >
          Agréguelos a una bolsa de infusión de 250 ml
        </Text>
        <View
          style={{
            flexDirection: 'row',
            width: '80%',
            alignSelf: 'center',
            // borderColor: color.generalBlue,
            // borderWidth: 1,
            // backgroundColor: color.noticeText,
            alignContent: 'space-between'
          }}
        >
          <Image
            source={require('../../assets/images/TransfM_3Prime.png')}
            style={{ marginTop: 10 }}
          />
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <Text
              style={{
                flex: 1,
                fontSize: 18,
                textAlign: 'center',
                marginTop: 20,
                marginRight: 10,
                flexWrap: 'wrap',
                borderBottomColor: color.kadcylaPrimary,
                borderBottomWidth: 1

                // width: '80%'
              }}
            >
              NaCl al 0.45%
            </Text>
            <Text
              style={{
                flex: 1,
                fontSize: 18,
                textAlign: 'center',
                marginRight: 10,
                flexWrap: 'wrap',
                marginTop: 10
                // width: '80%'
              }}
            >
              NaCl al 0.9%
              <Text
                style={{ color: color.kadcylaPrimary }}
              >{`\nRequiere filtro`}</Text>
            </Text>
          </View>
        </View>
        {/* <View style={{ height: layout.window.height * 0.12 }}></View> */}
        {layout.window.height >= 812 && (
          <View style={{ height: layout.window.height * 0.12 }} />
        )}
        <NavigationButtons
          navigation={this.props.navigation}
          anterior={{ visible: true, view: 'view3' }}
          siguiente={{ visible: true, view: 'view6' }}
          colors={[color.kadcylaPrimary, color.kadcylaSecondary]}
          // params={{
          //   dilucion: this.state.dilucion,
          //   dosis: { d00: this.state.d00, d60: this.state.d60 }
          // }}
        />
        <View style={{ marginBottom: 30 }} />

        <Toast
          ref='toast'
          style={{
            width: layout.window.width,
            height: layout.window.height * 0.15,
            backgroundColor: color.noticeText
          }}
          position='top'
          positionValue={0}
          fadeInDuration={750}
          fadeOutDuration={3000}
          // opacity={0.95}
          defaultCloseDelay={100}
        />
      </ScrollView>
      //   <View
      //     style={{
      //       position: 'absolute',
      //       width: '100%',
      //       marginTop: layout.window.height - layout.window.height * 0.25,
      //       marginBottom: 20
      //     }}
      //   >

    //   </View>
    // </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#a13388',
    fontSize: 22,
    textAlign: 'center'
  },
  text: {
    width: '45%',
    backgroundColor: '#FFFFFF',
    elevation: 10,
    borderRadius: 25,
    borderColor: 'rgba(0,0,0,0)',
    marginBottom: 16,
    marginTop: 15
  },
  text2: {
    alignSelf: 'center',
    textAlign: 'center',
    width: '80%',
    fontSize: 18,
    marginTop: 15
  },
  list: {
    width: '80%',
    borderRadius: 25,
    elevation: 10,
    height: '25%'
  }
})
