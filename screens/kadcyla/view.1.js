import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Input, ListItem } from 'react-native-elements'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import Coeficiente from '../../constants/Coeficiente'
import Icon from 'react-native-vector-icons/FontAwesome'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'

export default class MainKadcyla extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Kadcyla',
      colors: [color.kadcylaPrimary, color.kadcylaSecondary],
      navigation: navigation
    })

  constructor (props) {
    super(props)
    this.state = { dosis: 0, peso: 0 }
  }

  calculaDosis = peso => {
    let dosis = Coeficiente.k_dosis * parseInt(peso)
    dosis = !isNaN(dosis) ? dosis : 0
    this.setState({ dosis: dosis, peso: peso })
  }

  calculaPeso = dosis => {
    let peso = 0
    if (dosis > 0) {
      peso = Number.parseFloat(parseInt(dosis) / Coeficiente.k_dosis).toFixed(2)
    }
    this.setState({ dosis: dosis, peso: peso })
  }

  render () {
    let { dosis, peso } = this.state
    return (
      <ScrollView>
        <Text h3 style={styles.title}>
          Cantidad de Frascos
        </Text>
        <Text style={styles.text2}>Ingrese la dosis única recomendada</Text>
        <View
          style={{
            flexGrow: 1,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Input
            containerStyle={styles.text}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            placeholder='Dosis...'
            keyboardType='numeric'
            rightIcon={
              <Text style={{ marginRight: 20, fontSize: 18 }}>mg</Text>
            }
            onChangeText={value => this.calculaPeso(value)}
            // value={!isNaN(dosis) ? Number.parseFloat(dosis).toFixed(2).toString() : '0'}
            value={dosis.toString()}
            returnKeyType={'done'}
          />
          <Text h5 style={styles.text2}>
            O seleccione el peso del paciente{' '}
          </Text>
          <Input
            containerStyle={styles.text}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            placeholder='Peso...'
            keyboardType='numeric'
            rightIcon={
              <Text style={{ marginRight: 20, fontSize: 18 }}>Kg</Text>
            }
            onChangeText={value => this.calculaDosis(value)}
            value={peso.toString()}
            returnKeyType={'done'}
          />
          {!!this.state.nameError && (
            <Text style={{ color: 'red' }}>{this.state.nameError}</Text>
          )}
        </View>
        <View
          style={{
            marginTop: layout.window.height - layout.window.height * 0.68
          }}
        >
          <TouchableOpacity
            style={{
              marginTop: 16,
              marginRight: 10,
              width: '40%',
              alignSelf: 'flex-end'
            }}
            onPress={() => {
              if (this.state.peso === 0 || isNaN(this.state.peso)) {
                this.setState(() => ({ nameError: 'Peso es requerido.' }))
              } else {
                this.setState(() => ({ nameError: null }))
                this.props.navigation.navigate('view2', {
                  dosis: this.state.dosis,
                  peso: this.state.peso
                })
              }
            }}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={[color.kadcylaPrimary, color.kadcylaSecondary]}
              style={{ borderRadius: 20 }}
            >
              <Text style={styles.buttonText}>Siguiente</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#a13388',

    textAlign: 'center'
  },
  text: {
    width: '45%',
    backgroundColor: '#FFFFFF',
    elevation: 10,
    borderRadius: 25,
    borderColor: 'rgba(0,0,0,0)',
    marginBottom: 16,
    marginTop: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84
  },
  text2: {
    textAlign: 'center',

    fontSize: 14,
    marginTop: 15
  },
  list: {
    width: '80%',
    borderRadius: 25,
    elevation: 10,
    height: '25%'
  },
  linearGradientSiguiente: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 20,
    marginTop: 16,
    width: '45%',
    marginRight: 10,
    elevation: 10,

    alignSelf: 'flex-end'
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
