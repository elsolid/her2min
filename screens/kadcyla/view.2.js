import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Input, ListItem } from 'react-native-elements'
import Icon from 'react-native-vector-icons/EvilIcons'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import Coeficiente from '../../constants/Coeficiente'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'

export default class MainKadcyla extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Kadcyla',
      colors: [color.kadcylaPrimary, color.kadcylaSecondary],
      navigation: navigation
    })

  constructor (props) {
    super(props)
    this.state = {
      frascos: { c00: 0, c60: 0 },
      dosis: this.props.navigation.getParam('dosis', ''),
      peso: this.props.navigation.getParam('peso', ''),
      dilucion: 0
    }
  }

  calculaDilucion = () => {
    const f00 = Coeficiente.k_00_ml
    const f60 = Coeficiente.k_60_ml
    let { dosis } = this.state

    let dilucion = dosis / Coeficiente.k_dilucion
    let aux = dilucion
    let frascos = { c00: 0, c60: 0 }

    while (dilucion > 0) {
      if (dilucion < f00) {
        frascos.c00 += 1
        dilucion -= f00
      } else if (dilucion < f60) {
        frascos.c60 += 1
        dilucion -= f60
      } else if (dilucion > f60) {
        frascos.c60 += 1
        dilucion -= f60
      } else {
        frascos.c00 += 1
        dilucion -= f00
      }
    }

    this.setState({ frascos: frascos, dilucion: aux })
  }

  calculaDosis = peso => {
    let dosis = Number.parseFloat(
      Coeficiente.k_dosis * parseFloat(peso)
    ).toFixed(2)
    dosis = !isNaN(dosis) ? dosis : 0
    this.setState({ dosis: dosis, peso: peso }, () => this.calculaDilucion())
  }

  calculaPeso = dosis => {
    let peso = 0
    if (dosis > 0) {
      peso = Number.parseFloat(parseFloat(dosis) / Coeficiente.k_dosis).toFixed(
        2
      )
    }
    this.setState({ dosis: dosis, peso: peso }, () => this.calculaDilucion())
  }

  render () {
    let { dosis, peso } = this.state
    const isIOS = Platform.OS === 'ios'

    return (
      <View>
        {/* <KeyboardAvoidingView
        // style={{ flex: 1 }}
        behavior={isIOS ? 'padding' : ''}
        enabled
        keyboardVerticalOffset={44}
      > */}
        <ScrollView
          style={{
            marginTop: 15,
            marginBottom: 20,
            height: layout.window.height * 0.75
          }}
        >
          <Text style={styles.title}>Cantidad de Frascos</Text>
          <Text style={styles.text2}>Ingrese la dosis única recomendada</Text>
          <View
            style={{
              flexGrow: 1,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Input
              containerStyle={styles.text}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Dosis...'
              keyboardType='numeric'
              rightIcon={
                <Text style={{ marginRight: 20, fontSize: 18 }}>mg</Text>
              }
              onChangeText={value => this.calculaPeso(value)}
              // value={!isNaN(dosis) ? Number.parseFloat(dosis).toFixed(2).toString() : '0'}
              value={dosis.toString()}
              returnKeyType={'done'}
              // onSubmitEditing={() => {
              //   if (this.state.peso === 0 || isNaN(this.state.peso) || this.state.peso === '') {
              //     this.setState(() => ({ nameError: 'Peso es requerido.' }))
              //   } else {
              //     this.setState(() => ({ nameError: null }))
              //     this.props.navigation.navigate('view3', {
              //       dilucion: this.state.dilucion,
              //       frascos: this.state.frascos
              //     })
              //   }
              // }}
            />
            {!!this.state.dosisError && (
              <Text style={{ color: 'red' }}>{this.state.dosisError}</Text>
            )}
            <Text h5 style={styles.text2}>
              O seleccione el peso del paciente{' '}
            </Text>
            <Input
              containerStyle={styles.text}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Peso...'
              keyboardType='numeric'
              rightIcon={
                <Text style={{ marginRight: 20, fontSize: 18 }}>Kg</Text>
              }
              onChangeText={value => this.calculaDosis(value)}
              value={peso.toString()}
              returnKeyType={'done'}
              // onSubmitEditing={() => {
              //   if (this.state.peso === 0 || isNaN(this.state.peso) || this.state.peso === '') {
              //     this.setState(() => ({ nameError: 'Peso es requerido.' }))
              //   } else {
              //     this.setState(() => ({ nameError: null }))
              //     this.props.navigation.navigate('view3', {
              //       dilucion: this.state.dilucion,
              //       frascos: this.state.frascos
              //     })
              //   }
              // }}
            />
            {!!this.state.nameError && (
              <Text style={{ color: 'red' }}>{this.state.nameError}</Text>
            )}
          </View>
          {layout.window.height >= 812  && (
            <View style={{ height:40 }} />
          )}
          <Text
            style={{
              width: '90%',
              alignSelf: 'center',
              margin: 20,
              fontSize: 18,
              textAlign: 'center',
              fontStyle: 'italic'
            }}
          >
            * En caso de una diferencia importante entre peso/dosis,
            <Text style={{ color: color.kadcylaPrimary }}>
              {' '}
              corroborar con el médico tratante
            </Text>
          </Text>
          {layout.window.height >= 812  && (
            <View style={{ height:40 }} />
          )}
          <Text
            style={{
              fontSize: 18,
              color: color.kadcylaTitle,
              textAlign: 'center'
            }}
          >
            Frascos a utilizar
          </Text>
          <View style={styles.frascos}>
            <Text h4 style={{ fontWeight: 'bold' }}>
              {this.state.frascos.c00} :{' '}
            </Text>
            <Text style={{ fontSize: 18 }}>frasco 100mg</Text>
          </View>
          <View style={{ marginBottom: 25, ...styles.frascos }}>
            <Text h4 style={{ fontWeight: 'bold' }}>
              {this.state.frascos.c60} :{' '}
            </Text>
            <Text style={{ fontSize: 18 }}>frasco 160mg</Text>
          </View>
        </ScrollView>
        <View
          style={{
            position: 'absolute',
            width: '100%',
            marginTop: layout.window.height - layout.window.height * 0.25,
            marginBottom: 20
          }}
        >
          <View
            style={{
              flexGrow: 1,
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <TouchableOpacity
              style={{
                // marginTop: 16,
                marginLeft: 10,
                width: '40%'
              }}
              onPress={() => this.props.navigation.navigate('Home')}
            >
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[color.kadcylaPrimary, color.kadcylaSecondary]}
                style={{ borderRadius: 20 }}
              >
                <Text style={styles.buttonText}>Anterior</Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                // marginTop: 16,
                marginRight: 10,
                width: '40%'
              }}
              onPress={() => {
                if (
                  this.state.dosis === 0 ||
                  isNaN(this.state.dosis) ||
                  this.state.dosis === ''
                ) {
                  this.setState(() => ({
                    dosisError: 'Dosis es requerido.',
                    nameError: 'Peso es requerido.'
                  }))
                } else if (
                  this.state.peso === 0 ||
                  isNaN(this.state.peso) ||
                  this.state.peso === ''
                ) {
                  this.setState(() => ({
                    dosisError: 'Dosis es requerido.',
                    nameError: 'Peso es requerido.'
                  }))
                } else {
                  this.setState(() => ({ nameError: null, dosisError: null }))
                  this.props.navigation.navigate('view3', {
                    dilucion: this.state.dilucion,
                    frascos: this.state.frascos
                  })
                }
              }}
            >
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[color.kadcylaPrimary, color.kadcylaSecondary]}
                style={{ borderRadius: 20 }}
              >
                <Text style={styles.buttonText}>Siguiente</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        {/* </KeyboardAvoidingView> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#a13388',
    fontSize: 22,
    textAlign: 'center'
  },
  text: {
    width: '45%',
    backgroundColor: '#FFFFFF',
    elevation: 10,
    borderRadius: 25,
    borderColor: 'rgba(0,0,0,0)',
    marginBottom: 16,
    marginTop: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84
  },
  text2: {
    textAlign: 'center',

    fontSize: 18,
    marginTop: 15
  },
  list: {
    width: '80%',
    borderRadius: 25,
    elevation: 10,
    height: '25%'
  },
  frascos: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
