import React, { Component } from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Input, ListItem, Image } from 'react-native-elements'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import HeaderNavigator from '../../components/HeaderNavigator'

// const image = require('../assets/images/kadcyla3.png')

export default class MainKadcyla extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Kadcyla',
      colors: [color.kadcylaPrimary, color.kadcylaSecondary],
      navigation: navigation
    })

  constructor(props) {
    super(props)
    this.state = {
      dilucion: this.props.navigation.getParam('dilucion', 0),
      frascos: this.props.navigation.getParam('frascos', 0)
    }
  }

  render() {
    return (
      <View>
        <ScrollView
          style={{
            marginTop: 15,
            marginBottom: 20,
            height: layout.window.height * 0.75
          }}
        >
          <Text style={styles.title}>Reconstitución</Text>
          <Text style={styles.text2}> Inyecte al vial agua estéril</Text>
          <View
            style={{
              flexGrow: 1,
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <View
              style={{
                width: '50%',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image source={require('../../assets/images/kadcyla4.4.png')} />
              <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 3 }}>
                5 ml
            </Text>
            </View>
            <View
              style={{
                width: '50%',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image source={require('../../assets/images/kadcyla4.4.png')} />
              <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 3 }}>
                8 ml
            </Text>
            </View>
          </View>
          <View
            style={{
              flexGrow: 1,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Image
              source={require('../../assets/images/kadcyla3.png')}
            style={{ marginLeft: 25 }}
            />
             <View
            style={{
              flexGrow: 1,
              flexDirection: 'row',
              justifyContent: 'center',
            }}
          >
            <View
              style={{
                width: '50%',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'lightgrey', marginLeft: 30 }}>
                (100 ml)
            </Text>
            </View>
            <View
              style={{
                width: '50%',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'lightgrey'}}>
                (160 ml)
            </Text>
            </View>
          </View>
            
            <View
              style={{
                width: '80%',
                alignItems: 'center',
                flexDirection: 'column',
                justifyContent: 'space-between',
                marginTop: 10
              }}
            >
              {layout.window.height >= 812 && <View style={{ height: 40 }} />}
              <Text style={{ textAlign: 'center', fontSize: 18, marginBottom: 20 }}>
                Mezcle suavemente la solución
                <Text
                  style={{
                    color: color.kadcylaPrimary,
                    fontSize: 18,
                    fontWeight: 'bold'
                  }}
                >{`\n¡No lo agite!`}</Text>
              </Text>
              <Image
                source={require('../../assets/images/kadcyla3.1.png')}
              // style={{ width: 200 }}
              />
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            position: 'absolute',
            width: '100%',
            marginTop: layout.window.height - layout.window.height * 0.25,
            marginBottom: 20
          }}
        >
          <NavigationButtons
            navigation={this.props.navigation}
            anterior={{ visible: true, view: 'view2' }}
            siguiente={{ visible: true, view: 'view4' }}
            colors={[color.kadcylaPrimary, color.kadcylaSecondary]}
            params={{
              dilucion: this.state.dilucion,
              frascos: this.state.frascos
            }}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#a13388',
    fontSize: 22,
    textAlign: 'center'
  },
  text: {
    width: '45%',
    backgroundColor: '#FFFFFF',
    elevation: 10,
    borderRadius: 25,
    borderColor: 'rgba(0,0,0,0)',
    marginBottom: 16,
    marginTop: 15
  },
  text2: {
    textAlign: 'center',

    fontSize: 18,
    marginTop: 15
  },
  list: {
    width: '80%',
    borderRadius: 25,
    elevation: 10,
    height: '25%'
  }
})
