import React, { Component } from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import {
  Button,
  Text,
  Input,
  ListItem,
  Image,
  Divider
} from 'react-native-elements'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import HeaderNavigator from '../../components/HeaderNavigator'
import Toast, { DURATION } from 'react-native-easy-toast'

// const image = require('../assets/images/kadcyla3.png')

export default class MainKadcyla extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Kadcyla',
      colors: [color.kadcylaPrimary, color.kadcylaSecondary],
      navigation: navigation
    })

  constructor(props) {
    super(props)
    this.state = {
      dilucion: this.props.navigation.getParam('dilucion', 0),
      dosis: this.props.navigation.getParam('dosis', 0)
    }
  }

  // componentDidMount = () => {
  //   this.refs.toast.show(
  //     <View
  //       style={{
  //         flexGrow: 1,
  //         height: layout.window.height * 0.13,
  //         backgroundColor: color.noticeText
  //       }}
  //     >
  //       <Text
  //         h3
  //         style={{
  //           textAlign: 'center',
  //           color: '#a13388',
  //           marginLeft: 15,
  //           marginRight: 15
  //         }}
  //       >
  //         Dilución
  //       </Text>
  //       <Text
  //         h5
  //         style={{
  //           textAlign: 'center',
  //           marginTop: 5,
  //           marginLeft: 15,
  //           marginRight: 15
  //         }}
  //       >
  //         Tome los mililitros señalados de cada frasco para la solución
  //       </Text>
  //     </View>
  //   )
  // }

  render() {
    return (
      // <View>
      <ScrollView
        // onScroll={() => {
        //   this.refs.toast.close()
        // }}
        style={{ marginTop: 15 }}
      >
        <View
          style={{
            flexGrow: 1,
            height: layout.window.height * 0.15
            // backgroundColor: color.noticeText
          }}
        >
          <Text
            h3
            style={{
              textAlign: 'center',
              color: '#a13388',
              marginLeft: 15,
              marginRight: 15
            }}
          >
            Dilución
            </Text>
          <Text
            h5
            style={{
              textAlign: 'center',
              fontSize: 18,
              marginTop: 5,
              marginLeft: 15,
              marginRight: 15
            }}
          >
            Tome los mililitros señalados de cada frasco para la solución
            </Text>
        </View>
        <Image
          source={require('../../assets/images/kadcyla4.png')}
          style={{ alignSelf: 'center', marginTop: 0 }}
        />
        <View
          style={{
            flexGrow: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            marginBottom: 15
          }}
        >
          <View
            style={{
              width: '50%',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Image source={require('../../assets/images/kadcyla4.4.png')} />
            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 3 }}>
              {Number.parseFloat(this.state.dosis.d60).toFixed(2)} ml
              </Text>
          </View>

          <View
            style={{
              width: '50%',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Image source={require('../../assets/images/kadcyla4.4.png')} />
            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 3 }}>
              {this.state.dosis.d00} ml
              </Text>
          </View>
        </View>
        <Text
          style={{
            alignSelf: 'center',
            textAlign: 'center',
            fontSize: 18,
            width: '65%'
          }}
        >
          Volumen total de la solución reconstituida a diluir
          </Text>
        <View
          style={{
            flexGrow: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            margin: 15
          }}
        >
          <Image source={require('../../assets/images/kadcyla4.4.png')} />
          <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 3 }}>
            {Number.parseFloat(this.state.dilucion).toFixed(2)} ml
            </Text>
        </View>
        <Text
          style={{
            alignSelf: 'center',
            textAlign: 'center',
            fontSize: 18,
            width: '65%'
          }}
        >
          Agréguelos a una bolsa de infusión de 250 ml
          </Text>
        <Image
          source={require('../../assets/images/kadcyla4.2.png')}
          style={{ alignSelf: 'center', marginTop: 5 }}
        />
        <NavigationButtons
          navigation={this.props.navigation}
          anterior={{ visible: true, view: 'view4' }}
          siguiente={{ visible: true, view: 'view6' }}
          colors={[color.kadcylaPrimary, color.kadcylaSecondary]}
        />
        <View style={{marginBottom: 30}}></View>
        {/* <Toast
          ref='toast'
          style={{
            width: layout.window.width,
            height: layout.window.height * 0.2,
            backgroundColor: '#fff'
          }}
          position='top'
          positionValue={0}
          fadeInDuration={750}
          fadeOutDuration={3000}
          opacity={0.95}
          defaultCloseDelay={100}
        /> */}
      </ScrollView>
      //   {/* <View
      //     style={{
      //       position: 'absolute',
      //       width: '100%',
      //       marginTop: layout.window.height - layout.window.height * 0.25,
      //       marginBottom: 20
      //     }}
      //   >

      // </View> */}
    )
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#a13388',

    textAlign: 'center'
  },
  text: {
    width: '45%',
    backgroundColor: '#FFFFFF',
    elevation: 10,
    borderRadius: 25,
    borderColor: 'rgba(0,0,0,0)',
    marginBottom: 16,
    marginTop: 15
  },
  text2: {
    textAlign: 'center',

    fontSize: 14,
    marginTop: 15
  },
  list: {
    width: '80%',
    borderRadius: 25,
    elevation: 10,
    height: '25%'
  }
})
