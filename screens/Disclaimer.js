import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../constants/Colors'
import Icon from 'react-native-vector-icons/EvilIcons'
import layout from '../constants/Layout'
import NavigationButtons from '../components/NavigationButtons'
import textos from '../constants/Textos'
import HeaderNavigator from '../components/HeaderNavigator'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

export default class SideIndicaciones extends Component {
  render () {
    return (
      <View style={{ flex: 1 }}>
        {/* <View style={{height:hp(40), backgroundColor: 'red'}}> */}
        <View
          style={{
            height: hp(76),
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}
        >
          <Text
            style={{
              marginTop: 10,
              fontSize: 26,
              fontWeight: 'bold',
              width: '65%',
              marginTop: 40,
              textAlign: 'center'
            }}
          >
            Disclaimer de responsabilidad
          </Text>
          <Text style={styles.text}>{textos.disclaimer}</Text>
          <TouchableOpacity
          style={{
            marginTop: 30,
            marginBottom: 40,
            width: '60%',
            alignSelf: 'center'
          }}
          onPress={() => {
            this.props.navigation.navigate('Home')
          }}
        >
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={[color.generalBlue, color.generalBlue]}
            style={{ borderRadius: 25 }}
          >
            <View
              style={{
                flexGrow: 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Text style={styles.buttonText}>Continuar</Text>
              {/* <View>
                    {this.state.showLoader ? (
                      <ActivityIndicator size='small' color='#ffffff' />
                    ) : null}
                  </View> */}
            </View>
          </LinearGradient>
         
        </TouchableOpacity>
        </View>
        
        <View
            style={{
              position: 'absolute',
              flex: 1,
              bottom: 0
            }}
          >
            <Image
              source={require('../assets/images/foot.png')}
              // style={{ width: 200 }}
            />
          </View>
        {/* </ScrollView>  */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 20,
    width: '85%',
    textAlign: 'center',
    fontSize: 18
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
