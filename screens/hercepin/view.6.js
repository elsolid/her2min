import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import Coeficiente from '../../constants/Coeficiente'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'
import Toast, { DURATION } from 'react-native-easy-toast'


export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Herceptin',
      subtitle: 'Intravenoso',
      colors: [color.hercepinPrimary, color.hercepinSecondary],
      navigation: navigation
    })

  constructor(props) {
    super(props)
    this.state = {
      dosis: this.props.navigation.getParam('dosis', 0),
      dilucion: 0
    }
  }

  componentDidMount = () => {
    let dilucion = this.state.dosis / Coeficiente.h_dilucion
    this.setState({ dilucion: dilucion })

    // this.refs.toast.show(
    //   <View
    //     style={{
    //       flexGrow: 1,
    //       height: layout.window.height * 0.12,
    //       backgroundColor: color.noticeText
    //     }}
    //   >
    //     <Text
    //       h3
    //       style={{
    //         textAlign: 'center',
    //         color: color.hercepinPrimary,
    //         marginLeft: 15,
    //         marginRight: 15,
    //         textTransform: 'uppercase'
    //       }}
    //     >
    //       No utilice solución dextrosa 5%
    //     </Text>
    //   </View>
    // )
  }

  render() {
    return (
      // <View>
      <ScrollView
        onScroll={() => {
          this.refs.toast.close()
        }}
        style={{ height: layout.window.height * 0.75 }}
      >
        <View
          style={{
            flexGrow: 1,
            // height: layout.window.height * 0.2,
            marginTop: 10,
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          <Text
            style={{
              textAlign: 'center',
              color: color.hercepinPrimary,
              fontSize: 22
            }}
          >
            Dilución
          </Text>
          <Text style={styles.text}>
            Volumen de la solución reconstituida a diluir
            <Text style={{ fontSize: 22, fontWeight: 'bold' }}>
              {`\n` + Number.parseFloat(this.state.dilucion).toFixed(2) + ' ml'}
            </Text>
          </Text>
          <Text style={{ color: color.hercepinPrimary, ...styles.text }}>¡No utilice solución dextrosa 5%!</Text>


          <Text style={styles.text}>Diluya en una bolsa de infusión</Text>
          <View
            style={{
              flexDirection: 'row',
              width: '80%',
              alignContent: 'space-between'
            }}
          >
            <Image
              source={require('../../assets/images/TransfusioN_3Prime.png')}
            // style={{ width: 200 }}
            />
            <Text
              style={{
                flex: 1,
                fontSize: 24,
                textAlign: 'center',
                marginTop: 20,
                marginRight: 10,
                marginBottom: 20,
                flexWrap: 'wrap'
                // width: '80%'
              }}
            >
              NaCl al 0.9% de 250 ml
            </Text>
          </View>
          <View
            style={{
              width: '80%',
              alignItems: 'center',
              flexDirection: 'column',
              justifyContent: 'space-between',
              marginTop: 30
            }}
          >
            <Text style={{ textAlign: 'center', fontSize: 18, marginBottom: 20 }}>
              Mezcle suavemente la solución
              <Text
                style={{
                  color: color.hercepinPrimary,
                  fontSize: 18,
                  fontWeight: 'bold'
                }}
              >{`\n¡No lo agite!`}</Text>
            </Text>
            <Image
              source={require('../../assets/images/hercepin.int.4.png')}
            // style={{ width: 200 }}
            />
          </View>
        </View>
        <View style={{ height: layout.window.height * 0.12 }} />

        <NavigationButtons
          navigation={this.props.navigation}
          anterior={{ visible: true, view: 'hercepin5' }}
          siguiente={{ visible: true, view: 'hercepin7' }}
          colors={[color.hercepinPrimary, color.hercepinSecondary]}
        />
        <View style={{ height: layout.window.height * 0.12 }} />

        <Toast
          ref='toast'
          style={{
            width: layout.window.width,
            height: layout.window.height * 0.15,
            backgroundColor: color.noticeText
          }}
          position='top'
          positionValue={0}
          fadeInDuration={750}
          fadeOutDuration={3000}
          // opacity={0.95}
          defaultCloseDelay={100}
        />
      </ScrollView>
      //   {/* <View
      //     style={{
      //       position: 'absolute',
      //       width: '100%',
      //       marginTop: layout.window.height - layout.window.height * 0.25,
      //       marginBottom: 20
      //     }}
      //   >

      //   </View>
      // </View> */}
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 30,
    width: '75%',
    textAlign: 'center',
    fontSize: 18
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
