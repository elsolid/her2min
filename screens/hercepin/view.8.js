import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Platform,
  KeyboardAvoidingView
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image, CheckBox, Input } from 'react-native-elements'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import Coeficiente from '../../constants/Coeficiente'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'

// tercera de subcutaneo
export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Herceptin',
      subtitle: 'Intravenoso',
      colors: [color.hercepinPrimary, color.hercepinSecondary],
      navigation: navigation
    })

  constructor(props) {
    super(props)
    this.state = {
      semanal: true,
      trimestral: false,
      dosis: '',
      peso: '',
      frasco: 0
    }
  }

  calculaDosis = (peso = this.state.peso) => {
    let dosis, frasco
    let { semanal } = this.state
    if (semanal) {
      dosis = Number.parseFloat(
        Coeficiente.h_sem_dosis * parseFloat(peso)
      ).toFixed(2)
      frasco = Math.ceil(dosis / 440)
    } else {
      dosis = Number.parseFloat(
        Coeficiente.h_tri_dosis * parseFloat(peso)
      ).toFixed(2)
      frasco = Math.ceil(dosis / 440)
    }
    dosis = !isNaN(dosis) ? dosis : 0
    this.setState({ dosis: dosis, peso: peso, frasco: frasco })
  }

  calculaPeso = (dosis = this.state.dosis) => {
    let peso = 0
    let frasco
    let { semanal } = this.state
    if (dosis > 0) {
      if (semanal) {
        peso = Number.parseFloat(
          parseFloat(dosis) / Coeficiente.h_sem_dosis
        ).toFixed(2)
        frasco = Math.ceil(dosis / 440)
      } else {
        peso = Number.parseFloat(
          parseFloat(dosis) / Coeficiente.h_tri_dosis
        ).toFixed(2)
        frasco = Math.ceil(dosis / 440)
      }
    }
    this.setState({ dosis: dosis, peso: peso, frasco: frasco })
  }

  render() {
    let { dosis, peso } = this.state
    const isIOS = Platform.OS === 'ios'

    return (
      <View>
        {/* <KeyboardAvoidingView behavior={isIOS ? 'padding' : ''} enabled keyboardVerticalOffset={44}> */}
        <ScrollView
          style={{ marginTop: 15, height: layout.window.height * 0.75 }}
        >
          <Text style={styles.title}>Dosis</Text>
          <CheckBox
            center
            title='Semanal'
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checkedColor={color.hercepinPrimary}
            checked={this.state.semanal}
            onPress={() => {
              this.setState(
                {
                  semanal: !this.state.semanal,
                  trimestral: !this.state.trimestral
                },
                () => this.calculaDosis()
              )
            }}
            containerStyle={{
              borderRadius: 20,
              alignItems: 'flex-start',
              marginTop: 10,
              width: '90%',
              alignSelf: 'center'
            }}
          />
          <CheckBox
            center
            title='Trimestral'
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checkedColor={color.hercepinPrimary}
            checked={this.state.trimestral}
            onPress={() => {
              this.setState(
                {
                  trimestral: !this.state.trimestral,
                  semanal: !this.state.semanal
                },
                () => this.calculaDosis()
              )
            }}
            containerStyle={{
              borderRadius: 20,
              alignItems: 'flex-start',
              width: '90%',
              alignSelf: 'center'
            }}
          />
          <View
            style={{
              flexGrow: 1,
              // height: layout.window.height * 0.2,
              marginTop: 10,
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'space-between'
            }}
          >
            <Text
              style={{
                textAlign: 'center',
                fontSize: 18
              }}
            >
              Ingrese la dosis de mantenimiento recomendada
            </Text>
            <Input
              containerStyle={{ ...styles.text, width: '70%' }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Dosis...'
              keyboardType='numeric'
              rightIcon={
                <Text style={{ marginRight: 20, fontSize: 18 }}>mg</Text>
              }
              // value={
              //   !isNaN(dosis)
              //     ? Number.parseFloat(dosis)
              //       .toFixed(2)
              //       .toString()
              //     : '0'
              // }
              onChangeText={value => this.calculaPeso(value)}
              value={dosis.toString()}
              returnKeyType={'done'}
            // onSubmitEditing={() => {
            //   if (this.state.peso === 0 || isNaN(this.state.peso) || this.state.peso === '') {
            //     this.setState(() => ({ nameError: 'Peso es requerido.' }))
            //   } else {
            //     this.setState(() => ({ nameError: null }))
            //     this.props.navigation.navigate('hercepin5', {
            //       dosis: this.state.dosis
            //     })
            //   }
            // }}
            />
            {!!this.state.dosisError && (
              <Text style={{ color: 'red' }}>{this.state.dosisError}</Text>
            )}
            <Text
              style={{
                textAlign: 'center',
                fontSize: 18
              }}
            >
              o ingrese el peso del paciente
            </Text>
            <Input
              containerStyle={{ ...styles.text, width: '70%' }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Peso...'
              keyboardType='numeric'
              rightIcon={
                <Text style={{ marginRight: 20, fontSize: 18 }}>Kg</Text>
              }
              onChangeText={value => this.calculaDosis(value)}
              value={peso.toString()}
              returnKeyType={'done'}
            // onSubmitEditing={() => {
            //   if (this.state.peso === 0 || isNaN(this.state.peso) || this.state.peso === '') {
            //     this.setState(() => ({ nameError: 'Peso es requerido.' }))
            //   } else {
            //     this.setState(() => ({ nameError: null }))
            //     this.props.navigation.navigate('hercepin5', {
            //       dosis: this.state.dosis
            //     })
            //   }
            // }}
            />
            {!!this.state.nameError && (
              <Text style={{ color: 'red' }}>{this.state.nameError}</Text>
            )}
            {layout.window.height >= 812 && (
              <View style={{ height: 30 }} />
            )}
            <Text
              style={{
                width: '90%',
                alignSelf: 'center',
                margin: 5,
                fontSize: 18,
                textAlign: 'center',
                fontStyle: 'italic'
              }}
            >
              * En caso de una diferencia importante entre peso/dosis,
              <Text style={{ color: color.hercepinPrimary }}>
                {' '}
                corroborar con el médico tratante
              </Text>
            </Text>
            {layout.window.height >= 812 && (
              <View style={{ height: 30 }} />
            )}
            <Text
              style={{
                fontSize: 18,
                color: color.hercepinPrimary,
                textAlign: 'center',
                marginTop: 10
              }}
            >
              Frascos recomendados
            </Text>
            <Text style={{ fontWeight: 'bold', fontSize: 18 }}>
              {!isNaN(this.state.frasco) ? this.state.frasco : 0}:
              <Text style={{ fontWeight: 'normal' }}>
                {' '}
                frasco(s) de (440mg)
              </Text>
            </Text>
          </View>
          <View style={{ marginBottom: 30 }}></View>
        </ScrollView>
        <View
          style={{
            position: 'absolute',
            width: '100%',
            marginTop: layout.window.height - layout.window.height * 0.22,
            marginBottom: 20
          }}
        >
          <View
            style={{
              flexGrow: 1,
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 30
            }}
          >
            <TouchableOpacity
              style={{
                // marginTop: 16,
                marginLeft: 10,
                width: '40%'
              }}
              onPress={() => this.props.navigation.navigate('hercepin1')}
            >
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[color.hercepinPrimary, color.hercepinSecondary]}
                style={{ borderRadius: 20 }}
              >
                <Text style={styles.buttonText}>Anterior</Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                // marginTop: 16,
                marginRight: 10,
                width: '40%'
              }}
              onPress={() => {
                if (
                  this.state.dosis === 0 ||
                  isNaN(this.state.dosis) ||
                  this.state.dosis === ''
                ) {
                  this.setState(() => ({
                    dosisError: 'Dosis es requerido.',
                    nameError: 'Peso es requerido.'
                  }))
                } else if (
                  this.state.peso === 0 ||
                  isNaN(this.state.peso) ||
                  this.state.peso === ''
                ) {
                  this.setState(() => ({
                    dosisError: 'Dosis es requerido.',
                    nameError: 'Peso es requerido.'
                  }))
                } else {
                  this.setState(() => ({ nameError: null, dosisError: null }))
                  this.props.navigation.navigate('hercepin5', {
                    dosis: this.state.dosis
                  })
                }
              }}
            >
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[color.hercepinPrimary, color.hercepinSecondary]}
                style={{ borderRadius: 20 }}
              >
                <Text style={styles.buttonText}>Siguiente</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
          
        </View>
        {/* <View style={{ height: 30 }}></View> */}
      </View>
      // {/* </KeyboardAvoidingView> */}
    )
  }
}

const styles = StyleSheet.create({
  title: {
    color: color.hercepinPrimary,
    fontSize: 22,
    textAlign: 'center'
  },
  text: {
    width: '45%',
    backgroundColor: '#FFFFFF',
    elevation: 10,
    borderRadius: 25,
    borderColor: 'rgba(0,0,0,0)',
    marginBottom: 16,
    marginTop: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
