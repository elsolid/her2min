import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../constants/Colors'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'
import layout from '../../constants/Layout'

export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Herceptin',
      colors: [color.hercepinPrimary, color.hercepinSecondary],
      navigation: navigation
    })

  render () {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <TouchableOpacity
          style={{
            width: 150,
            height: 150,
            borderRadius: 12,
            alignItems: 'center',
            justifyContent: 'center',
            margin: 15,
            borderRightWidth: 1,
            borderBottomWidth: 6,
            borderRightColor: color.tabIconDefault,
            borderBottomColor: color.hercepinPrimary,
            borderLeftWidth: 1,
            borderLeftColor: color.tabIconDefault
          }}
          onPress={() => this.props.navigation.navigate('hercepin8')}
        >
          <Image
            source={require('../../assets/images/hercepin1.png')}
            // style={{ width: 200 }}
          />
          <Text style={{fontSize: 22}}>Intravenoso</Text>
        </TouchableOpacity>
        {layout.window.height >= 812 && (
          <View style={{ height: 20 }} />
        )}
        <TouchableOpacity
          style={{
            width: 150,
            height: 150,
            borderRadius: 12,
            alignItems: 'center',
            justifyContent: 'center',
            margin: 15,
            borderRightWidth: 1,
            borderBottomWidth: 6,
            borderRightColor: color.tabIconDefault,
            borderBottomColor: color.hercepinPrimary,
            borderLeftWidth: 1,
            borderLeftColor: color.tabIconDefault
          }}
          onPress={() => this.props.navigation.navigate('hercepin2')}
        >
          <Image
            source={require('../../assets/images/hercepin1.1.png')}
            // style={{ width: 200 }}
          />
          <Text style={{fontSize: 22}}>Subcutáneo</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
