import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'

// tercera de subcutaneo
export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Herceptin',
      subtitle: 'Intravenoso',
      colors: [color.hercepinPrimary, color.hercepinSecondary],
      navigation: navigation
    })

  constructor(props) {
    super(props)
    this.state = {
      dosis: this.props.navigation.getParam('dosis', 0)
    }
  }

  render() {
    return (
      // <View>
      <ScrollView>
        <View
          style={{
            flexGrow: 1,
            // height: layout.window.height * 0.2,
            marginTop: 10,
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          <Text
            style={{
              textAlign: 'center',
              color: color.hercepinPrimary,
              fontSize: 22
            }}
          >
            Reconstitución
          </Text>
          <Text style={styles.text}>
            Inyecte lentamente el agua bacteriostática
          </Text>
          <View
            style={{
              flexGrow: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Image source={require('../../assets/images/hercepin4.4.png')} />
            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 3 }}>
              8 ml
          </Text>
          </View>
          <Image
            source={require('../../assets/images/hercepin.int.1.png')}
          // style={{ width: 200 }}
          />
           <View
            style={{
              flexGrow: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'lightgrey'}}>
              (440 ml)
          </Text>
          </View>
          <View
            style={{
              width: '80%',
              alignItems: 'center',
              flexDirection: 'column',
              justifyContent: 'space-between',
              marginTop: 20,
              marginBottom: 30
            }}
          >
            <Text style={{ textAlign: 'center', fontSize: 18, marginBottom: 20 }}>
              Movimiento rotatorio suave para disolver y espere 5 minutos
              <Text
                style={{ color: color.hercepinPrimary, fontSize: 18, fontWeight: 'bold' }}
              >{`\n¡No lo agite!`}</Text>
            </Text>
            {layout.window.height >= 812 && (
              <View style={{ height: 40 }} />
            )}
            <Image
              source={require('../../assets/images/hercepin.int.3.png')}
            // style={{ width: 200 }}
            />
          </View>
        </View>
        {layout.window.height >= 812 && (
          <View style={{ height: layout.window.height * 0.12 }} />
        )}
        {/* <View style={{ height: layout.window.height * 0.12 }}></View> */}
        <NavigationButtons
          navigation={this.props.navigation}
          anterior={{ visible: true, view: 'hercepin8' }}
          siguiente={{ visible: true, view: 'hercepin6' }}
          colors={[color.hercepinPrimary, color.hercepinSecondary]}
          params={{ dosis: this.state.dosis }}
        />
        <View style={{ marginBottom: 30 }}></View>

      </ScrollView>
      // {/* <View
      //   style={{
      //     position: 'absolute',
      //     width: '100%',
      //     marginTop: layout.window.height - layout.window.height * 0.25,
      //     marginBottom: 20
      //   }}
      // >

      // </View> */}
      // </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 10,
    width: '75%',
    textAlign: 'center',
    fontSize: 16
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
