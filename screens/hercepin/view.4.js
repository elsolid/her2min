import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'
import DisplayModal from '../../components/DisplayModal'

// tercera de subcutaneo
export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Herceptin',
      subtitle: 'Subcutáneo',
      colors: [color.hercepinPrimary, color.hercepinSecondary],
      navigation: navigation
    })

  state = {
    isModalVisible: true
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }

  render() {
    return (
      <ScrollView>
        {/* <DisplayModal
          // data='Krunal'
          display={this.state.isModalVisible}
          toggleModal={this.toggleModal}
          color={color.hercepinPrimary}
          navigation={this.props.navigation}
        /> */}
        <View
          style={{
            flexGrow: 1,
            // height: layout.window.height * 0.2,
            marginTop: 10,
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          <Text
            style={{
              textAlign: 'center',
              color: color.hercepinPrimary,
              fontSize: 22
            }}
          >
            Aplicación
          </Text>
          <Text style={styles.text}>
            {`Aplique en el tejido subcutáneo \n No en el muslo subyacente`}
          </Text>
          <Image
            source={require('../../assets/images/hercepin.sub.4.png')}
            style={{ marginTop: 15 }}
          />
          <Image
            source={require('../../assets/images/hercepin.sub.4.1.png')}
            style={{ marginTop: 10 }}
          />
          <View
            style={{
              flexGrow: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between'
            }}
          >
            <Text style={styles.text2}>{`Asepsia\n previa`}</Text>
            <Text style={styles.text2}>{`Jeringa\n paralela`}</Text>
            <Text style={styles.text2}>{`Tiempo de\n aplicación`}</Text>
          </View>
          <View style={{ width: '80%', backgroundColor: color.noticeText, alignItems: 'center' }}>
            <Text style={{ width: '90%', fontSize: 18, textAlign: 'center', margin: 10 }}>
              Si el paciente presenta dolor o molestias, reduzca la presión de la
              jeringa o denténgase
            <Text
                style={{ color: color.hercepinPrimary, fontWeight: 'bold' }}
              >{`\nsin retirar la jeringa.`}</Text>
            </Text>
            <Text style={{ marginBottom: 10, ...styles.text }}>
              Posterior a las molestias, continúe con la administración
          </Text>
          </View>
        </View>
        <View
          style={{
            flexGrow: 1,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 20,
            marginBottom: 15
          }}
        >
          <TouchableOpacity
            style={{
              marginTop: 16,
              marginLeft: 10,
              width: '40%'
            }}
            onPress={() => this.props.navigation.navigate('hercepin3')}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={[color.hercepinPrimary, color.hercepinSecondary]}
              style={{ borderRadius: 20 }}
            >
              <Text style={styles.buttonText}>Anterior</Text>
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              marginTop: 16,
              marginRight: 10,
              width: '40%'
            }}
            onPress={() => this.props.navigation.navigate('Home')}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={[color.hercepinPrimary, color.hercepinSecondary]}
              style={{ borderRadius: 20 }}
            >
              <Text style={styles.buttonText}>Terminar</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 10,
    width: '75%',
    textAlign: 'center',
    fontSize: 16
  },
  text2: {
    fontSize: 18,

    textAlign: 'center',
    marginBottom: 15,
    marginLeft: 15,
    marginRight: 15
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
