import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'
import Toast, { DURATION } from 'react-native-easy-toast'

// tercera de subcutaneo
export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Herceptin',
      subtitle: 'Subcutáneo',
      colors: [color.hercepinPrimary, color.hercepinSecondary],
      navigation: navigation
    })

  // componentDidMount = () => {
  //   this.refs.toast.show(
  //     <View
  //       style={{
  //         flexGrow: 1,
  //         height: layout.window.height * 0.12,
  //         backgroundColor: color.noticeText
  //       }}
  //     >
  //       <Text
  //         h3
  //         style={{
  //           textAlign: 'center',
  //           color: color.hercepinPrimary,
  //           marginLeft: 15,
  //           marginRight: 15
  //         }}
  //       >
  //         No utilice solución dextrosa 5%
  //       </Text>
  //     </View>
  //   )
  // }

  render() {
    return (
      <ScrollView
        // onScroll={() => {
        //   this.refs.toast.close()
        // }}
        style={{ marginBottom: 0 }}
      >
        <View
          style={{
            flexGrow: 1,
            // height: layout.window.height * 0.2,
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          {/* <View
            style={{
              flexGrow: 1,
              width: '100%',
              height: layout.window.height * 0.12,
              backgroundColor: color.noticeText
            }}
          >
            <Text
              h4
              style={{
                textAlign: 'center',
                color: color.hercepinPrimary,
                marginLeft: 25,
                marginRight: 25,
                marginTop: 15
              }}
            >
              No utilice solución dextrosa 5%
            </Text>
          </View> */}
          <Text

            style={{
              textAlign: 'center',
              color: color.hercepinPrimary,
              fontSize: 22,
              marginTop: 15,
              marginBottom: 10
            }}
          >
            Preparación
          </Text>
          {/* <Text style={styles.text}>
            1. Mantenga a temperatura ambiente antes de administrarlo
          </Text> */}
          {layout.window.height >= 812 && (
            <View style={{ height: 20 }} />
          )}
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              // borderColor: color.generalBlue,
              // borderWidth: 1,
              backgroundColor: color.noticeText,
              alignContent: 'space-between',
              alignItems: 'center',
              justifyContent: "center",
              paddingBottom: 10
            }}
          >
            <Image
              source={require('../../assets/images/hercepin.sub.2.png')}
              style={styles.imagen}
            />
            <Text
              style={{
                flex: 1,
                fontSize: 18,
                textAlign: 'center',
                justifyContent: 'center',
                marginTop: 20,
                marginRight: 10,
                marginBottom: 20,
                flexWrap: 'wrap',
                width: '80%'
              }}
            >Antes de administrarlo{'\n'}
              <Text style={{ fontWeight: 'bold', color: color.hercepinPrimary }}> mantenga a temperatura ambiente</Text>
            </Text>
          </View>
          {/* <Image
            source={require('../../assets/images/hercepin.sub.2.png')}
            style={styles.imagen}
          /> */}
          <Text style={styles.text}>
            Carge los 5 ml con una aguja hipodérmica de <Text style={{ color: color.hercepinPrimary }}>21G</Text>.
          </Text>
          {layout.window.height >= 812 && (
            <View style={{ height: 20 }} />
          )}
          {/* <Image
            source={require('../../assets/images/hercepin.sub.2.1.png')}
            style={styles.imagen}
          /> */}
          {/* <Text style={styles.text}>
            3. Remplace la aguja por una de 25G o 27G para la aplicación
          </Text> */}
          {layout.window.height >= 812 && (
            <View style={{ height: 20 }} />
          )}
          <Image
            source={require('../../assets/images/hercepin.sub.2.2.png')}
            style={styles.imagen}
          />
          <Text style={{ marginBottom: 20, ...styles.text }}>
            Para la aplicación reemplace la aguja por una de <Text style={{ color: color.hercepinPrimary }}>25G</Text> o <Text style={{ color: color.hercepinPrimary }}>27G</Text>.
          </Text>
        </View>
        {
          layout.window.height >= 812 && (
            <View style={{ height: layout.window.height * 0.12 }} />
          )
        }
        <NavigationButtons
          navigation={this.props.navigation}
          anterior={{ visible: true, view: 'hercepin2' }}
          siguiente={{ visible: true, view: 'hercepin4' }}
          colors={[color.hercepinPrimary, color.hercepinSecondary]}
        />
        <View style={{ marginBottom: 30 }} />

        {/* <Toast
          ref='toast'
          style={{
            width: layout.window.width,
            height: layout.window.height * 0.2,
            backgroundColor: '#fff'
          }}
          position='top'
          positionValue={0}
          fadeInDuration={750}
          fadeOutDuration={3000}
          opacity={0.95}
          defaultCloseDelay={100}
        /> */}
      </ScrollView >
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 10,
    width: '75%',
    textAlign: 'center',
    fontSize: 18
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  imagen: {
    marginTop: 20
  }
})
