import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../constants/Colors'
import Icon from 'react-native-vector-icons/EvilIcons'
import layout from '../../constants/Layout'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'

export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Herceptin',
      subtitle: 'Subcutáneo',
      colors: [color.hercepinPrimary, color.hercepinSecondary],
      navigation: navigation,
      headerStyle: {
        height: 300
      }
    })

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'center'
            }}
          >
            <Text style={{ color: color.hercepinPrimary, marginTop: 15, fontSize: 22 }}>
              Dosis
            </Text>
            {layout.window.height >= 812 && (
              <View style={{ height: 20 }} />
            )}
            <Text style={styles.text}>
              Dosis fija independiente del peso corporal
            </Text>
            {layout.window.height >= 812 && (
              <View style={{ height: 20 }} />
            )}
            <Image source={require('../../assets/images/hercepin.sub.1.png')} />
            {layout.window.height >= 812 && (
              <View style={{ height: 20 }} />
            )}
            <Text style={{ width: '90%', ...styles.text }}>
              {`5 ml de solución =\n`}
              <Text style={{ flex: 1, color: color.hercepinPrimary, fontSize: 22, fontWeight: 'bold' }}>
                600mg de trastuzumab
              </Text>
            </Text>
          </View>
        </ScrollView>
        <View style={{
          position: 'absolute',
          width: '100%',
          marginTop: layout.window.height - layout.window.height * 0.25,
          marginBottom: 20
        }}>
          <TouchableOpacity
            style={{
              // marginTop: 16,
              marginRight: 10,
              width: '40%',
              alignSelf: 'flex-end'
            }}
            onPress={() => this.props.navigation.navigate('hercepin3')}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={[color.hercepinPrimary, color.hercepinSecondary]}
              style={{ borderRadius: 20 }}
            >
              <Text style={styles.buttonText}>Siguiente</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 10,
    width: '70%',
    textAlign: 'center',
    fontSize: 16
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
