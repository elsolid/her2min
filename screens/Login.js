import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform
} from 'react-native'
import { Button, Image, Input, Text } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/FontAwesome'
import color from '../constants/Colors'
import Codigo from '../constants/Codigo'
import Especialistas from '../constants/Especialistas'
import DisplayModal from '../components/DisplayModalLogin'

export default class Login extends Component {
  static navigationOptions = {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      codigo: '',
      maxResult: '1000',
      nombre: 'ssss',
      paterno: '',
      materno: '',
      idCedula: '',
      showLoader: false,
      isModalVisible: false
    }

    this.inputs = {}
  }

  focusNextField = key => {
    this.inputs[key].focus()
  }

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      showLoader: false
    })
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'token')
    this.props.navigation.navigate('Disclaimer')
  }

  showLoader = () => {
    this.setState({ showLoader: true })
  }

  eliminarDiacriticos = texto => {
    return texto
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .toLowerCase()
  }

  handleCode = () => {
    let { codigo } = this.state
    return this.eliminarDiacriticos(codigo) === Codigo.codigo.toLowerCase()
  }

  handleEspecialistas = title => {
    let titulo = title.toLowerCase()
    let result = Especialistas.filter(x => titulo.indexOf(x.toLowerCase()) >= 0)
    return result.length > 0
  }

  handleSubmit = () => {
    this.showLoader()

    const url =
      'https://www.cedulaprofesional.sep.gob.mx/cedula/buscaCedulaJson.action'

    let data = new FormData()

    let { nombre, paterno, materno, idCedula } = this.state

    data.append(
      'json',
      JSON.stringify({
        maxResult: 1,
        nombre: this.eliminarDiacriticos(nombre),
        paterno: this.eliminarDiacriticos(paterno),
        materno: this.eliminarDiacriticos(materno),
        idCedula: this.eliminarDiacriticos(idCedula)
      })
    )

    let xhr = new XMLHttpRequest()

    let singIn = this._signInAsync
    let modal = this.toggleModal
    let especialista = this.handleEspecialistas

    if (this.handleCode() && this.state.codigo !== '') {
      singIn()
    } else if (!this.handleCode() && this.state.codigo !== '') {
      modal()
    } else {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === 4) {
          let { items, token } = JSON.parse(this.responseText)
          if (items.length > 0 && idCedula !== '') {
            if (especialista(items[0].titulo)) {
              singIn()
            } else {
              modal()
            }
          } else {
            modal()
          }
        }
      })

      xhr.open(
        'POST',
        'https://www.cedulaprofesional.sep.gob.mx/cedula/buscaCedulaJson.action'
      )
      xhr.setRequestHeader('cache-control', 'no-cache')
      xhr.setRequestHeader(
        'postman-token',
        '56461df9-bbc0-5212-12a9-a608f9f55c44'
      )

      xhr.send(data)
    }
  }

  render () {
    const isIOS = Platform.OS === 'ios'

    return (
      <KeyboardAvoidingView
        behavior={isIOS ? 'padding' : ''}
        enabled
        keyboardVerticalOffset={56}
      >
        <ScrollView>
          <DisplayModal
            data='No se encontraron coincidencias. Verifique los datos'
            display={this.state.isModalVisible}
            toggleModal={this.toggleModal}
            colors={[color.generalBlue, color.generalBlue]}
          />
          <View
            style={{
              flexGrow: 1,
              alignItems: 'center',
              flexDirection: 'column'
              // justifyContent:'flex-end'
            }}
          >
            <Image
              source={require('../assets/images/home.banner.png')}
              style={{
                alignSelf: 'center',
                marginTop: 0
              }}
            />
            <Text
              h3
              style={{ alignSelf: 'center', marginTop: 10, marginBottom: 20 }}
            >
              ¡Bienvenido!
            </Text>
            <Text style={{ fontSize: 18 }}>Ingrese código de acceso</Text>
            <Input
              containerStyle={{ ...styles.text, width: '70%' }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Código de acceso'
              leftIcon={
                <Icon
                  name='lock'
                  size={18}
                  color='grey'
                  style={styles.iconInput}
                />
              }
              onChangeText={value => this.setState({ codigo: value })}
            />
            <Text style={{ fontSize: 18, marginTop: 10 }}>
              O valide sus datos para acceder
            </Text>
            <Input
              containerStyle={{ ...styles.text, width: '70%' }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Nombre(s)'
              leftIcon={
                <Icon
                  name='user'
                  size={18}
                  color='grey'
                  style={styles.iconInput}
                />
              }
              onChangeText={value =>
                this.setState({ nombre: value, codigo: '' })
              }
              returnKeyType={'next'}
              ref={input => {
                this.inputs['nombre'] = input
              }}
              onSubmitEditing={() => {
                this.focusNextField('paterno')
              }}
              // blurOnSubmit={false}
            />
            <Input
              containerStyle={{ ...styles.text, width: '70%' }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Primer Apellido'
              leftIcon={
                <Icon
                  name='user'
                  size={18}
                  color='grey'
                  style={styles.iconInput}
                />
              }
              onChangeText={value =>
                this.setState({ paterno: value, codigo: '' })
              }
              ref={input => {
                this.inputs['paterno'] = input
              }}
              returnKeyType={'next'}
              onSubmitEditing={() => {
                this.focusNextField('materno')
              }}
              // blurOnSubmit={false}
            />
            <Input
              containerStyle={{ ...styles.text, width: '70%' }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Segundo Apellido'
              leftIcon={
                <Icon
                  name='user'
                  size={18}
                  color='grey'
                  style={styles.iconInput}
                />
              }
              onChangeText={value =>
                this.setState({ materno: value, codigo: '' })
              }
              ref={input => {
                this.inputs['materno'] = input
              }}
              returnKeyType={'next'}
              onSubmitEditing={() => {
                this.focusNextField('idCedula')
              }}
            />
            <Input
              containerStyle={{ ...styles.text, width: '70%' }}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='No. Cédula'
              keyboardType='numeric'
              leftIcon={
                <Icon
                  name='credit-card'
                  size={18}
                  color='grey'
                  style={styles.iconInput}
                />
              }
              onChangeText={value =>
                this.setState({ idCedula: value, codigo: '' })
              }
              ref={input => {
                this.inputs['idCedula'] = input
              }}
              returnKeyType={'done'}
              onSubmitEditing={() => {
                this.handleSubmit()
              }}
            />
            <TouchableOpacity
              style={{
                marginTop: 16,
                marginBottom: 40,
                width: '70%'
              }}
              onPress={this.handleSubmit}
              // onPress={this._signInAsync}
            >
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[color.generalBlue, color.generalBlue]}
                style={{ borderRadius: 25 }}
              >
                <View
                  style={{
                    flexGrow: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <Text style={styles.buttonText}>Empezar</Text>
                  <View>
                    {this.state.showLoader ? (
                      <ActivityIndicator size='small' color='#ffffff' />
                    ) : null}
                  </View>
                </View>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    width: '45%',
    backgroundColor: '#FFFFFF',
    elevation: 10,
    borderRadius: 25,
    borderColor: 'rgba(0,0,0,0)',
    marginBottom: 16,
    marginTop: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  iconInput: {
    marginRight: 15
  }
})
