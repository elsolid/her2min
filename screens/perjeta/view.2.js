import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text } from 'react-native-elements'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'
import Toast, { DURATION } from 'react-native-easy-toast'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

// tercera de subcutaneo
export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Perjeta',
      colors: [color.perjetaPrimary, color.perjetaSecondary],
      navigation: navigation
    })

  componentDidMount = () => {
    // this.refs.toast.show(
    //   <View
    //     style={{
    //       flexGrow: 1,
    //       height: layout.window.height * 0.12,
    //       backgroundColor: color.noticeText
    //     }}
    //   >
    //     <Text
    //       h3
    //       style={{
    //         textAlign: 'center',
    //         color: color.perjetaPrimary,
    //         marginLeft: 15,
    //         marginRight: 15,
    //         textTransform: 'uppercase'
    //       }}
    //     >
    //       No utilice solución dextrosa 5%
    //     </Text>
    //   </View>
    // )
  }

  render () {
    return (
      <View>
        <ScrollView
          onScroll={() => {
            this.refs.toast.close()
          }}
        >
          <View
            style={{
              flexGrow: 1,
              // height: layout.window.height * 0.2,
              marginTop: 15,
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'space-between'
            }}
          >
            <Text
              style={{
                color: color.perjetaPrimary,
                marginTop: 10,
                fontSize: 22
              }}
            >
              Dilución
            </Text>
            <Text style={{ ...styles.text, marginTop: 10 }}>
              Diluya en una bolsa de infusión
            </Text>
            <View
              style={{
                flexDirection: 'row',
                width: '80%',
                alignContent: 'space-between'
              }}
            >
              <Image
                source={require('../../assets/images/perjeta2.1.png')}
                // style={{ width: '25%', aspectRatio: 2/1 }}
              />
              <Text
                style={{
                  flex: 1,
                  fontSize: 24,
                  textAlign: 'center',
                  marginTop: 20,
                  marginRight: 10,
                  marginBottom: 20,
                  flexWrap: 'wrap'
                  // width: '80%'
                }}
              >
                NaCl al 0.9% de 250 ml
              </Text>
            </View>

            <View
              style={{
                width: '80%',
                alignItems: 'center',
                flexDirection: 'column',
                justifyContent: 'space-between',
                marginTop: 30
              }}
            >
              <Text
                style={{
                  alignSelf: 'center',
                  textAlign: 'center',
                  fontSize: 18,
                  marginBottom: 20,
                  color: color.perjetaPrimary
                }}
              >
                ¡No utilice solución dextrosa 5%!
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 18,
                  marginBottom: 20
                }}
              >
                Mezcle suavemente la solución
                <Text
                  style={{
                    color: color.perjetaPrimary,
                    fontSize: 18,
                    fontWeight: 'bold'
                  }}
                >{`\n¡No lo agite!`}</Text>
              </Text>
              {layout.window.height >= 812 && <View style={{ height: 40 }} />}
              <Image
                source={require('../../assets/images/perjeta2.png')}
                // style={{ width: 200 }}
              />
            </View>
          </View>

          <Toast
            ref='toast'
            style={{
              width: layout.window.width,
              height: layout.window.height * 0.15,
              backgroundColor: color.noticeText
            }}
            position='top'
            positionValue={0}
            fadeInDuration={750}
            fadeOutDuration={1000}
            // opacity={0.95}
            defaultCloseDelay={100}
          />
        </ScrollView>
        <View
          style={{
            position: 'absolute',
            width: '100%',
            marginTop: layout.window.height - layout.window.height * 0.25,
            marginBottom: 20
          }}
        >
          <NavigationButtons
            navigation={this.props.navigation}
            anterior={{ visible: true, view: 'perjeta1' }}
            siguiente={{ visible: true, view: 'perjeta3' }}
            colors={[color.perjetaPrimary, color.perjetaSecondary]}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 30,
    width: '75%',
    textAlign: 'center',
    fontSize: 18
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
