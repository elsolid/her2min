import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../constants/Colors'
import layout from '../../constants/Layout'
import Icon from 'react-native-vector-icons/EvilIcons'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'
import DisplayModal from '../../components/DisplayModal'

// tercera de subcutaneo
export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Perjeta',
      colors: [color.perjetaPrimary, color.perjetaSecondary],
      navigation: navigation
    })

  state = {
    isModalVisible: true
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }

  render () {
    return (
      // <View>
      <ScrollView>
        <DisplayModal
          // data='Krunal'
          display={this.state.isModalVisible}
          toggleModal={this.toggleModal}
          colors={[color.perjetaPrimary, color.perjetaSecondary]}
          navigation={this.props.navigation}
        />

        <View
          style={{
            flexGrow: 1,
            // height: layout.window.height * 0.2,
            marginTop: 15,
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          <Text
            style={{ color: color.perjetaPrimary, marginTop: 10, fontSize: 24 }}
          >
            Administración
          </Text>
          <Text style={{ fontSize: 18, fontWeight: 'bold', ...styles.text }}>
            1ra vez:
            <Text style={{ fontWeight: 'normal' }}>{` Infusión IV`}</Text>
          </Text>
          <View
            style={{
              flexGrow: 1,
              alignItems: 'center',
              justifyContent: 'space-between',
              flexDirection: 'row'
            }}
          >
            <Image
              source={require('../../assets/images/TransV_3Prime.png')}
              style={{ marginTop: 10, marginRight: 10 }}
            />
            <Image
              source={require('../../assets/images/R90V_3Prime.png')}
              style={{ marginTop: 10, marginLeft: 10 }}
            />
          </View>
          <Text style={{ fontSize: 18, fontWeight: 'bold', ...styles.text }}>
            Posteriores:
            <Text style={{ fontWeight: 'normal' }}>{` Infusión IV`}</Text>
          </Text>
          <View
            style={{
              flexGrow: 1,
              alignItems: 'center',
              justifyContent: 'space-between',
              flexDirection: 'row'
            }}
          >
            <Image
              source={require('../../assets/images/TransV_3Prime.png')}
              style={{ marginTop: 10, marginRight: 10 }}
            />
            <Image
              source={require('../../assets/images/R30V_3Prime.png')}
              style={{ marginTop: 10, marginLeft: 10 }}
            />
          </View>
          <View
            style={{
              width: '85%',
              alignItems: 'center',
              flexDirection: 'column',
              justifyContent: 'space-between',
              marginTop: 20
            }}
          >
            <Text
              style={{
                textAlign: 'center',
                fontSize: 18,
                margin: 20,
                width: '90%',
                fontStyle: 'italic'
              }}
            >
              Mantenga bajo observación por 90 y 30 minutos respectivamente
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: '80%',
              // borderColor: color.generalBlue,
              // borderWidth: 1,
              backgroundColor: color.noticeText,
              alignContent: 'space-between'
            }}
          >
            <Image
              source={require('../../assets/images/perjeta3.2.png')}
              style={{ marginTop: 10 }}
            />
            <Text
              style={{
                flex: 1,
                fontSize: 18,
                textAlign: 'center',
                color: color.perjetaPrimary,
                marginTop: 20,
                marginRight: 10,
                marginBottom: 20,
                flexWrap: 'wrap'
                // width: '80%'
              }}
            >
              <Text style={{ fontWeight: 'bold' }}>Disminuya </Text>o{' '}
              <Text style={{ fontWeight: 'bold' }}>suspenda</Text> la infusión
              en caso de
              <Text style={{ fontWeight: 'bold' }}> reacción</Text> durante la
              administración
            </Text>
          </View>
        </View>
        <View
          style={{
            flexGrow: 1,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 30,
            marginTop: 35
          }}
        >
          <TouchableOpacity
            style={{
              marginTop: 16,
              marginLeft: 10,
              width: '40%'
            }}
            onPress={() => this.props.navigation.navigate('perjeta2')}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={[color.perjetaPrimary, color.perjetaSecondary]}
              style={{ borderRadius: 20 }}
            >
              <Text style={styles.buttonText}>Anterior</Text>
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              marginTop: 16,
              marginRight: 10,
              width: '40%'
            }}
            onPress={() => this.props.navigation.navigate('Home')}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={[color.perjetaPrimary, color.perjetaSecondary]}
              style={{ borderRadius: 20 }}
            >
              <Text style={styles.buttonText}>Terminar</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
      //   <View
      //     style={{
      //       position: 'absolute',
      //       width: '100%',
      //       marginTop: layout.window.height - layout.window.height * 0.25,
      //       marginBottom: 20
      //     }}
      //   >

    //   </View>
    // </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 30,
    width: '75%',
    textAlign: 'center',
    fontSize: 18
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
