import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../constants/Colors'
import Icon from 'react-native-vector-icons/EvilIcons'
import layout from '../../constants/Layout'
import NavigationButtons from '../../components/NavigationButtons'
import HeaderNavigator from '../../components/HeaderNavigator'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

export default class MainHercepin extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Perjeta',
      colors: [color.perjetaPrimary, color.perjetaSecondary],
      navigation: navigation
    })

  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* <ScrollView style={{height:hp(40), backgroundColor: 'red'}}> */}
        <View
          style={{
            height: hp(76),
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}
        >
          <Text style={{ color: color.perjetaPrimary, marginTop: 10, fontSize: 22 }}>
            Dosis
            </Text>
          {layout.window.height >= 812 && (
            <View style={{ height: 20 }} />
          )}
          <Text style={styles.text}>
            Dosis fija independiente del peso corporal
            </Text>
          {layout.window.height >= 812 && (
            <View style={{ height: 20 }} />
          )}
          <Image source={require('../../assets/images/perjeta1.png')} />
          {layout.window.height >= 812 && (
            <View style={{ height: 20 }} />
          )}
          <Text style={{ fontSize: 18, fontWeight: 'bold', ...styles.text }}>
            1ra vez:
              <Text style={{ fontWeight: 'normal' }}>{` Infusión IV`}</Text>
          </Text>
          <Text style={{ color: color.perjetaPrimary, marginTop: 10, fontSize: 22 }}>
            840 mg
            </Text>
          {layout.window.height >= 812 && (
            <View style={{ height: 20 }} />
          )}
          <Text style={{ fontSize: 18, fontWeight: 'bold', ...styles.text }}>
            Posteriores:
              <Text style={{ fontWeight: 'normal' }}>{` Infusión IV`}</Text>
          </Text>
          <Text style={{ color: color.perjetaPrimary, marginTop: 10, fontSize: 22 }}>
            420 mg
            </Text>
        </View>
        {/* </ScrollView> */}
        <View
          style={{
            // position: 'absolute',
            width: '100%',
            height: hp(20)
            // marginTop: layout.window.height - layout.window.height * 0.25,
            // marginBottom: 20
          }}
        >
          <TouchableOpacity
            style={{
              // marginTop: 16,
              marginRight: 10,
              width: '40%',
              alignSelf: 'flex-end'
            }}
            onPress={() => this.props.navigation.navigate('perjeta2')}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={[color.perjetaPrimary, color.perjetaSecondary]}
              style={{ borderRadius: 20 }}
            >
              <Text style={styles.buttonText}>Siguiente</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 10,
    width: '70%',
    textAlign: 'center',
    fontSize: 18
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
