import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../../constants/Colors'
import Icon from 'react-native-vector-icons/EvilIcons'
import layout from '../../../constants/Layout'
import NavigationButtons from '../../../components/NavigationButtons'
import textos from '../../../constants/Textos'
import HeaderNavigator from '../../../components/HeaderNavigator'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

export default class SideIndicaciones extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Perjeta',
      colors: [color.perjetaPrimary, color.perjetaSecondary],
      navigation: navigation
    })

  render () {
    return (
      <View style={{ flex: 1 }}>
        {/* <ScrollView style={{height:hp(40), backgroundColor: 'red'}}> */}
        <ScrollView style={{ flex: 1 }}>
          <Text
            style={{
              color: color.perjetaPrimary,
              marginTop: 10,
              fontSize: 22,
              alignSelf: 'center'
            }}
          >
            Farmacovigilancia
          </Text>
          <Text style={styles.title}>
            Sabe que es la FARMACOVIGILANCIA{`\n`}
          </Text>
          <Text style={{ fontSize: 18, marginLeft: 20, marginRight: 20, ...styles.text}}>
            {textos.farmacovigilancia[0]}
            <Text style={styles.title}>
              {`\n`}
              {`\n`}
              {`\n`}
              ¿Por qué es importante Reportar los eventos adversos?
            </Text>
            {`\n`}
            {`\n`}
            {`\n`}
            {textos.farmacovigilancia[1]}
            {`\n`}
            {`\n`}
            {`\n`}
            {textos.farmacovigilancia[2]}
            {`\n`}
            {`\n`}
            {`\n`}
            {textos.farmacovigilancia[3]}
            {`\n`}
            {`\n`}
            {`\n`}
          </Text>
          {/* </View> */}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 10,
    width: '90%',
    textAlign: 'justify',
    fontSize: 18
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
    color: color.perjetaPrimary,
    marginTop: 15
  }
})
