import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../../constants/Colors'
import Icon from 'react-native-vector-icons/EvilIcons'
import layout from '../../../constants/Layout'
import NavigationButtons from '../../../components/NavigationButtons'
import textos from '../../../constants/Textos'
import HeaderNavigator from '../../../components/HeaderNavigator'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

export default class SideIndicaciones extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Perjeta',
      colors: [color.perjetaPrimary, color.perjetaSecondary],
      navigation: navigation
    })

  render () {
    return (
      <View style={{ flex: 1 }}>
        {/* <ScrollView style={{height:hp(40), backgroundColor: 'red'}}> */}
        <View
          style={{
            height: hp(76),
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}
        >
          <Text style={{ color: color.perjetaPrimary, marginTop: 10, fontSize: 22}}>
            Referencias
          </Text>
          <Text style={styles.text}>{textos.referencias}</Text>
        </View>
        {/* </ScrollView>  */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 10,
    width: '90%',
    textAlign: 'justify',
    fontSize: 18
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
