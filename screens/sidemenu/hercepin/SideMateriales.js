import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Button, Text, Image } from 'react-native-elements'
import color from '../../../constants/Colors'
import Icon from 'react-native-vector-icons/EvilIcons'
import layout from '../../../constants/Layout'
import NavigationButtons from '../../../components/NavigationButtons'
import textos from '../../../constants/Textos'
import HeaderNavigator from '../../../components/HeaderNavigator'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

export default class SideIndicaciones extends Component {
  static navigationOptions = ({ navigation }) =>
    HeaderNavigator({
      title: 'Herceptin',
      colors: [color.hercepinPrimary, color.hercepinSecondary],
      navigation: navigation
    })

  render () {
    return (
      <View style={{ flex: 1 }}>
        {/* <ScrollView style={{height:hp(40), backgroundColor: 'red'}}> */}
        <View
          style={{
            height: hp(76),
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}
        >
          <Text
            style={{ color: color.hercepinPrimary, marginTop: 10, fontSize: 22 }}
          >
            Materiales
          </Text>
          <Text style={{ fontSize: 18 }}>
            {textos.materiales.map(material => {
              return (
                <Text key={material}>
                  {`\n`}-{material}
                </Text>
              )
            })}
            </Text>
        </View>
        {/* </ScrollView>  */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    marginTop: 10,
    width: '95%',
    textAlign: 'left',
    fontSize: 18
  },
  buttonText: {
    fontSize: 18,

    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  }
})
