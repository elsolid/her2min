// Wherever you build your reducers
import { combineReducers, createStore, applyMiddleware, compose } from 'redux'
import AppNavigator from '../navigation/AppNavigator'

const navReducer = (state, action) => {
  const newState = AppNavigator.router.getStateForAction(action, state)
  return newState || state
}

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    nav: navReducer
  })

  // return store
  return createStore(rootReducer)
}
